/**
 * Created by gurpinder on 5/29/16.
 */

import { PRODUCTS } from '../../collections/products';
import { checkProductSubscription } from './checkProductSupscription';
import { Response } from '../response';
import { RBAC } from '../users/rbacChecks';

Meteor.methods({
    'getAllowedProducts': (userId) => {
        var loggedInUser = Meteor.userId();
        if(loggedInUser){
            var getsp = Meteor.users.findOne({_id:userId},{fields:{"profile.subscribedTo":1}});
            var sp = PRODUCTS.find({_id:{$in:getsp.profile.subscribedTo}}).fetch();
            return sp;
        }
        else{
            return;
        }
    },
    'allowProductAccess': (userId,product) => {
        var loggedInUser = Meteor.userId();
        var hasAccess  = checkProductSubscription.isSubscribedTo(product.name,loggedInUser);
        var alreadyHasAccess = checkProductSubscription.isSubscribedTo(product.name,userId);
        var canUpdate = RBAC.checkIfAdminOf(userId);
        let performUpdate = (id) => {
            var productId = product._id;
            var query = {'profile.subscribedTo':productId};
            Meteor.users.update(
                {_id:id},
                {$push:query}
            )
            return Response.success('Product access granted');
        }
        if(alreadyHasAccess){
            return Response.error('Access already granted');
        }
        if(Roles.userIsInRole(loggedInUser,'super-admin',Roles.GLOBAL_GROUP)){
           return performUpdate(userId);
        }
        if(hasAccess && canUpdate){
            return performUpdate(userId);
        }
        else{
            return Response.error('Access denied');
        }
    },
    'denyProductAccess': (userId,product) => {
        var loggedInUser = Meteor.userId();
        var hasAccess  = checkProductSubscription.isSubscribedTo(product.name,loggedInUser);
        var alreadyHasAccess = checkProductSubscription.isSubscribedTo(product.name,userId);
        var canUpdate = RBAC.checkIfAdminOf(userId);
        let performUpdate = (id) => {
            var productId = product._id;
            var query = {'profile.subscribedTo':productId};
            Meteor.users.update(
                {_id:id},
                {$pull:query}
            )
            return Response.success('Product access now denied to user');
        }
        if(!alreadyHasAccess){
            return Response.error('Access already denied');
        }
        if(Roles.userIsInRole(loggedInUser,'super-admin',Roles.GLOBAL_GROUP)){
            return performUpdate(userId);
        }
        if(hasAccess && canUpdate){
            return performUpdate(userId);
        }
        else{
            return Response.error('Access denied');
        }
    }
})