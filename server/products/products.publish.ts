/**
 * Created by d3v on 5/24/16.
 */

import { PRODUCTS } from '../../collections/products';

Meteor.publish('products',function(){
    var loggedInUser = this.userId;
    var group = Roles.getGroupsForUser(loggedInUser,['admin']);
    if(Roles.userIsInRole(loggedInUser,'super-admin',Roles.GLOBAL_GROUP)){
        return PRODUCTS.find();
    }
    if(group){
        var subscribedProducts = Meteor.users.findOne({_id:loggedInUser},{fields:{"profile.subscribedTo":1}});
        var sp = PRODUCTS.find({_id:{$in:subscribedProducts.profile.subscribedTo}});
        return sp;
    }
    else{
        this.stop();
        return;
    }
})
