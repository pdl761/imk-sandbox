/**
 * Created by d3v on 5/24/16.
 */

import { PRODUCTS } from '../../collections/products.ts';

export let checkProductSubscription = {
    isSubscribedTo: (product:any,user:any)=>{
        if(!user || !product){
            return false;
        }
        let _user = Meteor.users.findOne({_id:user},{fields:{"profile.subscribedTo":1}});
        let _subscribed = _user.profile.subscribedTo;
        let _product = PRODUCTS.findOne({name:product});
        let isSubscribedToProduct: boolean = false;
        for(var s in _subscribed){
            if(_product._id === _subscribed[s]){
                isSubscribedToProduct = true;
            }
        }
        return isSubscribedToProduct;
    }
}