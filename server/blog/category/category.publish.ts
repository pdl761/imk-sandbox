/**
 * Created by gurpinder on 5/15/16.
 */

import { CATEGORIES } from '../../../collections/category';

Meteor.publish('categories',function(){
    var isLoggedIn = this.userId;
    if(Roles.userIsInRole(isLoggedIn,'super-admin',Roles.GLOBAL_GROUP)){
        return CATEGORIES.find();
    }
    if(isLoggedIn){
        return CATEGORIES.find({userId:isLoggedIn});
    }
    else{
        this.stop();
        return;
    }
});