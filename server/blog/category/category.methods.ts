/**
 * Created by gurpinder on 5/15/16.
 */

import { CATEGORIES } from '../../../collections/category';
import { GROUPS } from '../../../collections/groups.ts';
import { Response } from '../../response.ts';
import { checkProductSubscription } from '../../products/checkProductSupscription';
import { IMKProducts } from '../../products/products.const';
import {IMK} from "../../../client/app/shared/imk-functions/imk";

Meteor.methods({
    'createCategory': (category) => {
        let currentLoggedInUser = Meteor.userId();
        let _groups = Roles.getGroupsForUser(currentLoggedInUser,'admin');
        let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,currentLoggedInUser);
        for(let i=0;i<_groups.length;i++){
            let _g = (_groups[i]).replace(/\_/g,'.');
            if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP) || Roles.userIsInRole(currentLoggedInUser,'admin',_g) && isSubscribedToProduct){
                let _categoryExists = CATEGORIES.findOne({title:category,userId:currentLoggedInUser});
                if(_categoryExists){
                    return  Response.error('Category ' + category + ' already exists');
                }
                else{
                    CATEGORIES.insert({
                        "title":category,
                        "userId":currentLoggedInUser,
                        "createdAt":new Date().valueOf(),
                        "isDeleted": false
                    });
                    return Response.success("Category created successfully");
                }
            }
            else{
                return Response.error('Access Denied');
            }
        }
    },
    'deleteCategory': (categoryId) => {
        let currentLoggedInUser = Meteor.userId();
        let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,currentLoggedInUser);
        if(Roles.userIsInRole(currentLoggedInUser, 'super-admin', Roles.GLOBAL_GROUP)){
            CATEGORIES.remove({_id:categoryId});
            return Response.success('Category Deleted',null);
        }
        else {
            let _groups = Roles.getGroupsForUser(currentLoggedInUser, 'admin');
            for (let i = 0; i < _groups.length; i++) {
                let _g = (_groups[i]).replace(/\_/g, '.');
                if (Roles.userIsInRole(currentLoggedInUser, 'admin', _g) && isSubscribedToProduct) {
                    let _category = CATEGORIES.findOne({_id: categoryId, userId: currentLoggedInUser});
                    if (_category) {
                        CATEGORIES.remove({_id: categoryId});
                        return Response.success('Category Deleted');
                    }
                    else {
                        return Response.error('Category not found');
                    }
                }
                else {
                    return Response.error('Access Denied');
                }
            }

        }
    }
});