/**
 * Created by gurpinder on 5/15/16.
 */

import { COMMENTS } from '../../../collections/comments';
import { POSTS } from '../../../collections/posts';
import { checkProductSubscription } from '../../products/checkProductSupscription';
import { IMKProducts } from '../../products/products.const';

Meteor.publish('getPostComments',function(param){
    var isLoggedIn = this.userId;
    let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,isLoggedIn);
    if(Roles.userIsInRole(isLoggedIn,'super-admin',Roles.GLOBAL_GROUP) || isLoggedIn && isSubscribedToProduct){
        return COMMENTS.find({param});
    }
    else{
        this.stop();
        return;
    }

})