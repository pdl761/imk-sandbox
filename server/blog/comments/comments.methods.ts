/**
 * Created by gurpinder on 5/15/16.
 */

import { COMMENTS } from '../../../collections/comments';
import { POSTS } from '../../../collections/posts';
import { Response } from '../../response.ts';
import { checkProductSubscription } from '../../products/checkProductSupscription';
import { IMKProducts } from '../../products/products.const';

Meteor.methods({
    'postComment' : (postId,comment) =>{
        let commentsAllowed = POSTS.findOne({_id:postId,allowComments:true});
        let _loggedInUser = Meteor.userId();
        let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,_loggedInUser);
        if(_loggedInUser && isSubscribedToProduct) {
            if (commentsAllowed) {
                COMMENTS.insert({
                    postId: postId,
                    userName: comment.userName,
                    userEmail: comment.userEmail,
                    comment: comment.comment,
                    createdAt: new Date().valueOf(),
                    isApproved: false,
                    isDeleted: false
                })
                return Response.success('Comment added successfully. Will be published once approved');
            }
            else {
                return Response.error('Comments are not allowed on this post');
            }
        }
        else{
            return Response.error('Access denied');
        }

    },
    'delComment': (commentId,postId) =>{
        let _loggedInUser = Meteor.userId();
        let _isPostOwner = POSTS.findOne({_id:postId,userId:_loggedInUser});
        let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,_loggedInUser);
        if(Roles.userIsInRole(_loggedInUser,'super-admin',Roles.GLOBAL_GROUP) || _isPostOwner && isSubscribedToProduct){
            COMMENTS.remove({_id:commentId});
            return Response.success('Comment removed');
        }
        else{
            return Response.error('Access denied');
        }
    },
    'approveComment': (commentId,postId) => {
        let _loggedInUser = Meteor.userId();
        let _isPostOwner = POSTS.findOne({_id:postId,userId:_loggedInUser});
        let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,_loggedInUser);
        if(Roles.userIsInRole(_loggedInUser,'super-admin',Roles.GLOBAL_GROUP) || _isPostOwner && isSubscribedToProduct){
            COMMENTS.update(commentId,{
                $set: {isApproved: true}
            });
            return Response.success('Comment approved');
        }
        else{
            return Response.error('Access denied');
        }
    },
    'unApproveComment': (commentId,postId) => {
        let _loggedInUser = Meteor.userId();
        let _isPostOwner = POSTS.findOne({_id:postId,userId:_loggedInUser});
        let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,_loggedInUser);
        if(Roles.userIsInRole(_loggedInUser,'super-admin',Roles.GLOBAL_GROUP) || _isPostOwner && isSubscribedToProduct){
            COMMENTS.update(commentId,{
                $set: {isApproved: false}
            });
            return Response.success('Comment un-approved');
        }
        else{
            return Response.error('Access denied');
        }
    },
    'getPostComments': (postId) => {
        let _loggedInUser = Meteor.userId();
        let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,_loggedInUser);
        if(Roles.userIsInRole(_loggedInUser,'super-admin',Roles.GLOBAL_GROUP) || _loggedInUser && isSubscribedToProduct){
            return COMMENTS.find({postId:postId}).fetch();
        }
    }
});
