/**
 * Created by d3v on 5/23/16.
 */

import { RestApi } from '../api.ts';
import { GROUPS } from '../../collections/groups';
import { POSTS } from  '../../collections/posts'
import { COMMENTS } from '../../collections/comments';
import { CATEGORIES } from '../../collections/category';
import { checkProductSubscription } from '../products/checkProductSupscription';
import { IMKProducts } from '../products/products.const';

RestApi.addRoute('posts/:userId/:group', {authRequired: false}, {
    get:  function () {
        let category = this.params.category;
        //return category;
        let group = this.urlParams.group;
        let user = this.urlParams.userId;
        let posts = POSTS.find({groups:{$in:[group]}}).fetch();
        let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,user);
        for(let i=0;i<posts.length;i++){
            let user = Meteor.users.findOne({_id:posts[i].userId},{fields:{username:1,emails:1,profile:1}});
            let categories = CATEGORIES.findOne({_id:posts[i].categoryId},{fields:{title:1}});
            let noOfComments = COMMENTS.find({postId:posts[i]._id,isApproved:true}).count();
            delete posts[i]['userId'];
            delete posts[i]['categoryId'];
            posts[i].user = user;
            posts[i].category = categories;
            posts[i].commentsCount = noOfComments;
        }


        //let userIds = posts.map(function(p) { return p.userId });
        //let posts = POSTS.aggregate([
        //    {$match: {groups:{$in: [group]}}},
        //    {$project:{userId:1}},
        //    {$unwind:"userId"},
        //    {}
        //])
        //let users = Meteor.users.find({_id:{$in:userIds}}).fetch();
        if(!isSubscribedToProduct){
            return {success:false,message:'Product not subscribed', data:{}}
        }
        if(posts){
            return {success: true, data : {posts}}
        }
        return {
            success: false, message: 'No posts'
        }
    }
});

RestApi.addRoute('post/:id', {authRequired: false}, {
    get:  function () {
        let id = this.urlParams.id;
        let posts = POSTS.findOne({_id:id});
        let userId = posts.userId;
        let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,userId);
        let user = Meteor.users.findOne({_id:posts.userId},{fields:{username:1,emails:1,profile:1}});
        let comments = COMMENTS.find({postId:id,isApproved:true},{fields:{username:1,userEmail:1,comment:1,createdAt:1}}).fetch();
        if(!isSubscribedToProduct){
            return {success:false,message:'Product not subscribed', data:{}}
        }
        if(posts){
            return {success: true, data : {posts,comments,user}}
        }
        return {
            success: false, message: 'No posts'
        }
    }
});

RestApi.addRoute('addComment/:postId',{authRequired:false},{
    post: function(){
        let postId = this.urlParams.postId;
        let _canComment = POSTS.findOne({_id:postId,allowComments:true});
        let userId ;
        if(_canComment){
            userId = _canComment.userId;
        }

        let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,userId);
        let _comment = {
            postId: postId,
            userId: null,
            userName: this.bodyParams.username,
            userEmail: this.bodyParams.email,
            comment: this.bodyParams.comment,
            createdAt:new Date().valueOf(),
            isApproved: false,
            isDeleted: false
        }
        if(!isSubscribedToProduct){
            return {success:false,message:'Product not subscribed', data:{}}
        }
        if(_canComment){
            COMMENTS.insert(_comment);
            return {success: true, data: {message: 'Comment added.'}}
        }
        else{
            return {success: false, data: {message: 'Comment are not allowed on this post.'}}
        }
    }
});