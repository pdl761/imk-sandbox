/**
 * Created by gurpinder on 5/15/16.
 */

import { POSTS } from '../../../collections/posts';
import { GROUPS } from '../../../collections/groups.ts';
import { checkProductSubscription } from '../../products/checkProductSupscription';
import { IMKProducts } from '../../products/products.const';
import { RBAC } from '../../users/rbacChecks';

Meteor.publish('posts',function(){
    var isLoggedIn = this.userId;
    let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,isLoggedIn);
    let isUser = RBAC.getUserRoleAndGroup(isLoggedIn);
    if(Roles.userIsInRole(isLoggedIn,'super-admin',Roles.GLOBAL_GROUP)){
        return  POSTS.find();
    }
    if(isLoggedIn && isSubscribedToProduct && isUser.role === 'user'){
        return POSTS.find({groups:{$in:[isUser.group]}})
    }
    if(isLoggedIn && isSubscribedToProduct){
        return POSTS.find({userId:isLoggedIn});
    }
    else{
        this.stop();
        return;
    }
});

