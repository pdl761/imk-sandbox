/**
 * Created by gurpinder on 5/15/16.
 */

import { POSTS } from '../../../collections/posts';
import { GROUPS } from '../../../collections/groups.ts';
import { Response } from '../../response.ts';
import { Thumbs } from '../../../collections/+api/image/collection';
import { checkProductSubscription } from '../../products/checkProductSupscription';
import { IMKProducts } from '../../products/products.const';
import { RBAC } from '../../users/rbacChecks';

Meteor.methods({
    'createPost': (postBody,post) => {
        let currentLoggedInUser = Meteor.userId();
        let createPost = () => {
            var _fimg = post.featuredImageId;
            var thumb = Thumbs.findOne({originalId:_fimg});
            POSTS.insert({
                title: post.title,
                featuredImageId: post.featuredImageId,
                featuredImage: post.featuredImage,
                featuredImageThumb: thumb.url,
                body: postBody,
                createdAt: new Date().valueOf(),
                tags:post.tags,
                categoryId:post.categoryId,
                userId:currentLoggedInUser,
                isPublished: post.isPublished,
                isDeleted: post.isDeleted,
                allowComments: post.allowComments,
                isFeatured: post.isFeatured,
                groups: post.groups
            });
            return Response.success("Post created successfully");
        }
        let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,currentLoggedInUser);
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP)){
            return createPost();
        }
        if(currentLoggedInUser){
            let _groups = Roles.getGroupsForUser(currentLoggedInUser,'admin');
            for(let i=0;i<_groups.length;i++){
                let _g = (_groups[i]).replace(/\_/g,'.');
                if(Roles.userIsInRole(currentLoggedInUser,'admin',_g) && isSubscribedToProduct) {
                   return createPost();
                }
                else{
                    return Response.error('Access denied');
                }
            }
        }
        else{
            return Response.error('Access denied');
        }
    },
    'updatePost': (postId,postBody,post) => {
        let currentLoggedInUser = Meteor.userId();
        let isPostOwner = POSTS.findOne({_id:postId,userId:currentLoggedInUser});
        let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,currentLoggedInUser);
        let performUpdate = () =>{
            var _fimg = post.featuredImageId;
            var thumb = Thumbs.findOne({originalId:_fimg});
            POSTS.update(postId,{
                $set:{
                    title: post.title,
                    featuredImageId: post.featuredImageId,
                    featuredImage: post.featuredImage,
                    featuredImageThumb: thumb.url,
                    body: postBody,
                    updatedAt:new Date().valueOf(),
                    tags:post.tags,
                    categoryId:post.categoryId,
                    userId:currentLoggedInUser,
                    isPublished: post.isPublished,
                    isDeleted: post.isDeleted,
                    allowComments: post.allowComments,
                    isFeatured: post.isFeatured,
                    groups: post.groups
                }
            });
            return Response.success("Post updated successfully");
        }
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP) || isPostOwner && isSubscribedToProduct){
            return performUpdate();
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'deletePost': (postId) => {
        let currentLoggedInUser = Meteor.userId();
        let isPostOwner = POSTS.findOne({_id:postId,userId:currentLoggedInUser});
        let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,currentLoggedInUser);
        if(Roles.userIsInRole(currentLoggedInUser, 'super-admin', Roles.GLOBAL_GROUP) || isPostOwner && isSubscribedToProduct){
            POSTS.remove({_id:postId});
            return Response.success('Post Deleted');
        }
        else {
            return Response.error('Access Denied');
        }
    },
    'getPost': (postId) => {
        var isLoggedIn = Meteor.userId();
        let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.BLOG,isLoggedIn);
        let isUser = RBAC.getUserRoleAndGroup(isLoggedIn);
        if(Roles.userIsInRole(isLoggedIn,'super-admin',Roles.GLOBAL_GROUP)){
            return  POSTS.findOne({_id:postId});
        }
        if(isLoggedIn && isSubscribedToProduct && isUser.role === 'user'){
            return POSTS.findOne({_id:postId,groups:{$in:[isUser.group]}});
        }
        if(isLoggedIn && isSubscribedToProduct){
            return POSTS.findOne({_id:postId,userId:isLoggedIn});
        }
        else{
            this.stop();
            return;
        }
    }
});