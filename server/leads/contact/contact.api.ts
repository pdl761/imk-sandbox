/**
 * Created by d3v on 5/23/16.
 */

import { RestApi } from '../../api';
import { LEADS } from '../../../collections/leads';

RestApi.addRoute('leads/contact/:userId', {authRequired: false}, {
    post:  function () {
        var userId = this.urlParams.userId;
        var name = this.bodyParams.name;
        var email = this.bodyParams.email;
        var phone = this.bodyParams.phone;
        var message = this.bodyParams.message;
        var validation = [];
        if(!userId){
           validation.push("Invalid id");
        }
        if(!name){
            validation.push("Name is required");
        }
        if(!email){
            validation.push("Email is required");
        }
        if(!message){
            validation.push("Message is required");
        }
        if(validation.length>0){
            return {success: false, message: validation};
        }

        var userExists = Meteor.users.findOne({_id:userId});
        if(userExists){
            var userEmail = userExists.emails[0].address;
            var lead = {
                userName: name,
                userEmail:email,
                userPhone: phone,
                userMessage: message,
                userId: userId,
                type: 'contact',
                meta: {},
                createdAt: new Date().valueOf(),
                isDeleted: false
            }
             var _leadData = LEADS.insert(lead);
            var emailMessage = message + "\n\n" + "Phone: " + phone + "\n" + "Email: " + email;
            if(_leadData){
                Email.send({
                    to: userEmail,
                    from: 'admin@imkloud.com',
                    subject: 'LEADS - Message from ' + name,
                    text: emailMessage
                });
                return {success: true, message: ["Thanks for contacting us. We will get back to you as soon as possible"]};
            }
            else{
                return {success: false, message: ["Oops! something went wrong. Please try again later"]};
            }

        }
        else{
            return {success: false, message: ["Invalid id"]};
        }



    }

});

RestApi.addRoute('pear/contact/:userId', {authRequired: false}, {
    post:  function () {
        var userId = this.urlParams.userId;
        var name = this.bodyParams.name;
        var email = this.bodyParams.email;
        var phone = this.bodyParams.phone;
        var address = this.bodyParams.address;
        var unit = this.bodyParams.unit;
        var city = this.bodyParams.city;
        var state = this.bodyParams.state;
        var zip = this.bodyParams.zip;

        var validation = [];
        if(!userId){
            validation.push("Invalid id");
        }
        if(!address){
            validation.push("Address is required");
        }
        if(!phone){
            validation.push("Phone is required");
        }
        if(validation.length>0){
            return {success: false, message: validation};
        }

        var userExists = Meteor.users.findOne({_id:userId});
        if(userExists){
            var userEmail = userExists.emails[0].address;



            var emailMessage = 'Name: ' + name +
                                "\n" +
                                "Phone: " + phone +
                                "\n" +
                                "Email: " + email +
                                "\n" +
                                "Address: " + address +
                                "\n" +
                                "Unit: " + unit +
                                "\n" +
                                "City: " + city +
                                "\n" +
                                "State: " + state +
                                "\n" +
                                "Zip: " + zip;

                Email.send({
                    to: userEmail,
                    from: 'pear@interosfeastbay.com',
                    subject: 'PEAR - Message from ' + name,
                    text: emailMessage
                });
                return {success: true, message: ["Thanks for contacting us. We will get back to you as soon as possible"]};
        }
        else{
            return {success: false, message: ["Invalid id"]};
        }



    }

});