/**
 * Created by d3v on 5/23/16.
 */

import { LEADS } from '../../../collections/leads';
import { Response } from '../../response';
import { checkProductSubscription } from '../../products/checkProductSupscription';
import { IMKProducts } from '../../products/products.const';

Meteor.methods({
    'deleteLeadContact': (contactId) => {
        let _currentLoggedInUser = Meteor.userId();
        let isLeadOwner = LEADS.findOne({_id:contactId,userId:_currentLoggedInUser});
        let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.LEADS,_currentLoggedInUser);
        if(Roles.userIsInRole(_currentLoggedInUser, 'super-admin', Roles.GLOBAL_GROUP) || isLeadOwner && isSubscribedToProduct){
            LEADS.remove({_id:contactId});
            return Response.success('Message Deleted');
        }
        else {
            return Response.error('Access Denied');
        }
    }
})