/**
 * Created by d3v on 5/23/16.
 */

import { LEADS } from '../../../collections/leads';
import { checkProductSubscription } from '../../products/checkProductSupscription';
import { IMKProducts } from '../../products/products.const';

Meteor.publish('leadsContact',function(){
    var isLoggedIn = this.userId;
    let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.LEADS,isLoggedIn);
    if(Roles.userIsInRole(isLoggedIn,'super-admin',Roles.GLOBAL_GROUP)){
        return  LEADS.find();
    }
    if(isLoggedIn && isSubscribedToProduct){
        return LEADS.find({userId:isLoggedIn});
    }
    else{
        this.stop();
        return;
    }
});

Meteor.publish('getLeadContact',function(param){
    var isLoggedIn = this.userId;
    let isSubscribedToProduct : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.LEADS,isLoggedIn);
    if(Roles.userIsInRole(isLoggedIn,'super-admin',Roles.GLOBAL_GROUP)){
        return  LEADS.find({param});
    }
    if(isLoggedIn && isSubscribedToProduct){
        return LEADS.find({param,userId:isLoggedIn});
    }
    else{
        this.stop();
        return;
    }
});