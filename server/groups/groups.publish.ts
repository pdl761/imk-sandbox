/**
 * Created by gurpinder on 5/15/16.
 */

import { GROUPS } from '../../collections/groups.ts';

Meteor.publish('groups',function(){
    var isLoggedIn = this.userId;
    if(Roles.userIsInRole(isLoggedIn,'super-admin',Roles.GLOBAL_GROUP)){
        return GROUPS.find({title:{$ne:'www.imkloud.com'}});
    }
    else{
        this.stop();
        return false;
    }
});