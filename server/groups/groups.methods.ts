/**
 * Created by gurpinder on 5/15/16.
 */

import { GROUPS } from '../../collections/groups.ts';
import { Response } from '../response.ts';

Meteor.methods({
    'createGroup': (group) => {
        let currentLoggedInUser = Meteor.userId();
        group = group.trim();
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP)){
            let groupExists = GROUPS.findOne({title:group});
            if(groupExists){
                return Response.error('Group ' + group + ' already exists');
            }
            else{
                GROUPS.insert({
                    "title":group,
                    "userId":currentLoggedInUser,
                    "createdAt": new Date().valueOf(),
                    "isDeleted": false
                });
                return Response.success("Group created successfully");
            }
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'deleteGroup': (groupId) => {
        let currentLoggedInUser = Meteor.userId();
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP)){
            let group = GROUPS.findOne({_id:groupId});
            if(group){
                let groupToFind = "roles."+(group.title).replace(/\./g,'_');
                let query = {};
                query[groupToFind] = {$exists:true};
                let groupInUse = Meteor.users.findOne(query, {fields:{_id:1}});
                if(groupInUse){
                    return Response.error("Group is in use");
                }
                else{
                    GROUPS.remove({_id:groupId,userId:currentLoggedInUser});
                    return Response.success('Group deleted successfully');
                }
            }
            else{
                return Response.error('Group not found');
            }
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'getGroupsForUser': () => {
        var isLoggedIn = Meteor.userId();
        if(isLoggedIn){
            let _groups = Roles.getGroupsForUser(isLoggedIn,'admin');
            let groups = [];
            for(let i=0;i<_groups.length;i++) {
                let _g = (_groups[i]).replace(/\_/g, '.');
                groups.push({title:_g});
            }
            return groups;
        }
    }
});