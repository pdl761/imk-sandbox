/**
 * Created by d3v on 5/19/16.
 */

import { Images } from '../../collections/+api/image/collection';

Meteor.publish('uploadedImageList',function(){
    var isLoggedIn = this.userId;
    if(isLoggedIn){
        return Images.find({userId:isLoggedIn},{fields:{url:1}});
    }
    else{
        this.stop();
        return false;
    }
})