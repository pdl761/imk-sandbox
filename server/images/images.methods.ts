/**
 * Created by d3v on 5/19/16.
 */

import { Images } from '../../collections/+api/image/collection';
import { Response } from "../response";

Meteor.methods({
    'delUploadedImages': (id) => {
        let currentLoggedInUser = Meteor.userId();
        let owner = Images.findOne({_id:id,userId:currentLoggedInUser});
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP) || currentLoggedInUser === owner.userId) {
                Images.remove({_id:id});
                return Response.success('Image deleted successfully');
        }
        else{
            return Response.error('Access Denied');
        }

    }
})