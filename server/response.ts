/**
 * Created by gurpinder on 5/14/16.
 */

export let Response = {
    error:(msg:String)=>{
        return {success: false,status:"error", message: msg }
    },
    success:(msg:String,data?:any)=>{
        if(!data){
            data = [];
        }
        return {success: true,status:200, message: msg,data:data }
    }
}


