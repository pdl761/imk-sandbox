/**
 * Created by d3v on 6/1/16.
 */

import { RBAC } from '../users/rbacChecks';
import { MEDIA_KIT } from  '../../collections/mediaKit/mediakit';
import { MEDIA_KIT_CATEGORIES } from '../../collections/mediaKit/categories';
import { Response } from '../response';
import { Thumbs } from '../../collections/+api/image/collection';
import { checkProductSubscription } from '../products/checkProductSupscription';
import { IMKProducts } from "../products/products.const";
import { MEDIA_KIT_PACKAGES } from "../../collections/mediaKit/package";
import { Images } from "../../collections/+api/image/collection";
import { USER_BRANCHES } from "../../collections/userBranchs";
import { USER_TYPES } from "../../collections/usertypes";
import { USER_ORDERS } from "../../collections/userOrders";

Meteor.methods({
    'addMediaKit':(media)=>{
        let loggedInUser = Meteor.user();
        let isAdmin: Boolean = RBAC.isAdmin(loggedInUser._id);
        console.log(loggedInUser.profile.userType);
        let addMedia = (media) =>{
            let categoryTitle = MEDIA_KIT_CATEGORIES.findOne({_id:media.categoryId});
            if(!categoryTitle){
                return Response.error('Invalid Category');
            }
            var _fimg = media.mediaKitImgId;
            var thumb = Thumbs.findOne({originalId:_fimg});

            let _media = {
                title: media.title,
                description: media.description,
                categoryId:media.categoryId,
                imageId: media.mediaKitImgId,
                isDeleted:false,
                userId:loggedInUser._id,
                groups: media.groups,
                addedAt:new Date().valueOf(),
                updatedAt:new Date().valueOf(),
                sizeAndPrice:media.sizeAndPrice,
                userType: loggedInUser.profile.userType
            }
            MEDIA_KIT.insert(_media);
            return Response.success('Media Kit added successfully');
        }
        let isSubscribed : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.MEDIA_KIT,loggedInUser._id);
        if(Roles.userIsInRole(loggedInUser._id,'super-admin',Roles.GLOBAL_GROUP) || isAdmin && isSubscribed){
            return addMedia(media);
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'getMediaKitWithVendorsAndBranches':(id:string)=>{
        let loggedInUser = Meteor.userId();
        let userOrg = RBAC.getUserRoleAndGroup(loggedInUser);
        let vendor = USER_TYPES.findOne({name:'vendor'});
        let group = (userOrg.group).replace(/\./g,'_');
        let query = {"profile.userType":vendor._id};
        if(query){
            query["roles."+group] = {$exists:true,$ne:null}
        }
        var user = Meteor.users.find(query,{fields:{_id:1,username:1,emails:1,profile:1}}).fetch();
        //query["roles."+group] = {$exists:true,$ne:null};

        ////let join = {
        ////    transform: function(m){
        ////        m.branchs = USER_BRANCHES.find({group:userOrg.group}).fetch()
        ////        return m;
        ////    }
        ////};
        //let userOrg = RBAC.getUserRoleAndGroup(loggedInUser);
        //let vendor = USER_TYPES.findOne({name:'vendor'});
        //console.log(vendor);
        //let group = (userOrg.group).replace(/\./g,'_');
        //let query = {'profile.userType':vendor._id};
        ////query["roles."+group] = {$exists:true,$ne:null};
        //
        //var branchAndVendor = Meteor.users.find(query,{fields:{profile:1}});
        //console.log(branchAndVendor);
        //return branchAndVendor;
        let join = {
            transform: function(m){
                m.category = MEDIA_KIT_CATEGORIES.findOne({_id:m.categoryId},{fields:{_id:1,title:1}});
                m.image = Images.findOne({'_id':m.imageId},{fields:{originalId:1,url:1}});
                m.branches = USER_BRANCHES.find({group:userOrg.group}).fetch();
                m.vendors = user;
                return m;
            }
        };
        var media = MEDIA_KIT.findOne({_id:id},join);
        return media;
    },
    'getMediaKitById':(id)=>{
        let loggedInUser = Meteor.userId();
        let mediaId: String = id;
        let join = {
            transform: function(m){
                m.category = MEDIA_KIT_CATEGORIES.findOne({_id:m.categoryId},{fields:{_id:1,title:1}});
                m.image = Images.findOne({'_id':m.imageId},{fields:{originalId:1,url:1}});
                return m;
            }
        };
        if(loggedInUser){
            var media  = MEDIA_KIT.findOne({_id:mediaId},join);
            return media;
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'removeMediaKitById':(id)=>{
        let loggedInUser = Meteor.userId();
        let mediaId: String = id;
        let isAdmin: Boolean = RBAC.isAdmin(loggedInUser);
        let removeMedia = (id) => {
            MEDIA_KIT.update({_id:id},{
                $set:{
                    isDeleted: true
                }
            });
            return Response.success('Media Kit removed successfully');
        }
        let isOwner = MEDIA_KIT.findOne({_id:mediaId,userId:loggedInUser});
        let isSubscribed : boolean = checkProductSubscription.isSubscribedTo(IMKProducts.MEDIA_KIT,loggedInUser);
        if(Roles.userIsInRole(loggedInUser,'super-admin',Roles.GLOBAL_GROUP)){
            return removeMedia(mediaId);
        }
        if(isAdmin && isOwner && isSubscribed){
            return removeMedia(mediaId);
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'addMediaKitPackage':(mkPackage)=>{
        let loggedInUser = Meteor.userId();
        let isAdmin: Boolean = RBAC.isAdmin(loggedInUser);
        let createPackage = (p) => {
            let _package = {
                title: p.title,
                package: p.package,
                createdAt: new Date().valueOf(),
                userId: loggedInUser,
                isDeleted:false
            }
            MEDIA_KIT_PACKAGES.insert(_package);
            return Response.success('Media Kit package created successfully');
        }
        if(Roles.userIsInRole(loggedInUser,'super-admin',Roles.GLOBAL_GROUP) || isAdmin){
            return createPackage(mkPackage);
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'removeMediaKitPackage':(id)=>{
        let loggedInUser = Meteor.userId();
        let isAdmin: Boolean = RBAC.isAdmin(loggedInUser);
        let isOwner = MEDIA_KIT_PACKAGES.findOne({_id:id});
        let removePackage = (pid) => {
            MEDIA_KIT_PACKAGES.remove({_id:pid});
            return Response.success('Media Kit package removed successfully');
        }
        if(Roles.userIsInRole(loggedInUser,'super-admin',Roles.GLOBAL_GROUP) || isAdmin && isOwner.userId === loggedInUser){
            return removePackage(id);
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'getBranchAndVendor':()=>{
        //let loggedInUser = Meteor.userId();
        ////let join = {
        ////    transform: function(m){
        ////        m.branchs = USER_BRANCHES.find({group:userOrg.group}).fetch()
        ////        return m;
        ////    }
        ////};
        //let userOrg = RBAC.getUserRoleAndGroup(loggedInUser);
        //let vendor = USER_TYPES.findOne({name:'vendor'});
        //console.log(vendor);
        //let group = (userOrg.group).replace(/\./g,'_');
        //let query = {'profile.userType':vendor._id};
        ////query["roles."+group] = {$exists:true,$ne:null};
        //
        //var branchAndVendor = Meteor.users.find(query,{fields:{profile:1}});
        //console.log(branchAndVendor);
        //return branchAndVendor;
        return "hello";

    },
    'placeOrderMediaKit':(order)=>{
        let loggedInUser = Meteor.userId();
        let placeOrder = (order) => {
            let userOrg = RBAC.getUserRoleAndGroup(loggedInUser);
            let group = userOrg.group;
            let orders = [];
            for(var i=0;i < order.length;i++){
                orders.push({
                        mediaKitId: order[i].itemId,
                        requestedChanges: order[i].requestedChanges,
                        size: order[i].size,
                        quantity: order[i].quantity,
                        branchId: order[i].branch,
                        vendorId: order[i].vendor,
                        status: 0,
                        message: 'Thanks for your order.',
                        group: group
                    })
                }
            let userOrders = {
                userId: loggedInUser,
                orders: orders,
                orderedAt: new Date().valueOf(),
                orderedItemsCount: order.length,
                isDeleted: false,
                group: group
            }
            USER_ORDERS.insert(userOrders);
            return Response.success('Your order is placed successfully. You can track your order under My ORDERS menu. Thank you')
        }
        if(loggedInUser){
            return placeOrder(order);
        } else {
            return Response.error('Access Denied');
        }

    },
    'approveOrderItem':(orderId:String,itemId:String)=>{
        var loggedInUser = Meteor.userId();
        var order = USER_ORDERS.findOne({_id:orderId});
        let approveOrder= () =>{
            USER_ORDERS.update({_id:orderId,'orders.mediaKitId':itemId},{
                $set:{
                    'orders.$.status': 1
                }
            });
            return Response.success('Order item approved successfully');;
        }
        if(Roles.userIsInRole(loggedInUser,'super-admin',Roles.GLOBAL_GROUP) || Roles.userIsInRole(loggedInUser,'admin',order.group)){
            return approveOrder();
        } else {
            return Response.error('Access Denied');
        }
    },
    'rejectOrderItem':(orderId:String,itemId:String)=>{
        var loggedInUser = Meteor.userId();
        var order = USER_ORDERS.findOne({_id:orderId});
        let declineOrder= () =>{
            USER_ORDERS.update({_id:orderId,'orders.mediaKitId':itemId},{
                $set:{
                    'orders.$.status': 2
                }
            });
            return Response.success('Order item declined successfully');
        }
        if(Roles.userIsInRole(loggedInUser,'super-admin',Roles.GLOBAL_GROUP) || Roles.userIsInRole(loggedInUser,'admin',order.group)) {
            return declineOrder();
        } else {
            return Response.error('Access Denied');
        }
    },
    'getOrderDetailById':(orderId: String)=>{
        var loggedInUser = Meteor.userId();
        var _order = USER_ORDERS.findOne({_id:orderId});
        let getDetails = (id) => {
            let join = {
                transform : function(order){
                    for(var o in order.orders){
                        order.orders[o].mediaKitDetails = MEDIA_KIT.findOne(
                            {_id:order.orders[o].mediaKitId},
                            {fields:{_id:1,title:1,description:1,imageId:1,categoryId:1,sizeAndPrice:1}}
                        );
                        let image = Thumbs.findOne(
                            {originalId:order.orders[o].mediaKitDetails.imageId},
                            {fields:{_id:1,url:1}}
                        );
                        let category = MEDIA_KIT_CATEGORIES.findOne(
                            {_id:order.orders[o].mediaKitDetails.categoryId},
                            {fields:{_id:1,title:1}}
                        );
                        let branch;
                        if(order.orders[o].branchId !== 'download'){
                            branch = USER_BRANCHES.findOne(
                                {_id:order.orders[o].branchId},
                                {fields: {_id:1,name:1,address:1,contact:1}}
                            );
                        }
                        let vendor;
                        if(order.orders[o].vendorId !== 'any'){
                            vendor = Meteor.users.findOne(
                                {_id:order.orders[o].vendorId},
                                {fields:{_id:1,username:1,profile:1,emails:1}}
                            )
                        }
                        order.orders[o].mediaKitDetails.imageThumb = image.url;
                        order.orders[o].mediaKitDetails.categoryTitle = category.title;
                        order.orders[o].branchDetails = branch;
                        order.orders[o].vendorDetails = vendor;
                    }
                    let user = Meteor.users.findOne(
                        {_id:order.userId},
                        {fields:{_id:1,username:1,emails:1,profile:1}}
                    )
                    order.userDetails = user;
                    return order;
                }
            }
            let ORDER = USER_ORDERS.find({_id:id},join).fetch();
            return Response.success('Success',ORDER);
        }
        if(Roles.userIsInRole(loggedInUser,'super-admin',Roles.GLOBAL_GROUP) || Roles.userIsInRole(loggedInUser,'admin',_order.group) || _order.userId === loggedInUser){
            return getDetails(orderId);
        } else {
            return Response.error('Access Denied');
        }
    },
})