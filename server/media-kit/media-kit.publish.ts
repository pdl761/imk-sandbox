/**
 * Created by d3v on 6/1/16.
 */

import { RBAC } from '../users/rbacChecks';
import { MEDIA_KIT_CATEGORIES } from '../../collections/mediaKit/categories';
import { MEDIA_KIT } from "../../collections/mediaKit/mediakit";
import { MEDIA_KIT_PACKAGES } from "../../collections/mediaKit/package";
import { POSTS } from '../../collections/posts';
import { COMMENTS } from '../../collections/comments';
import { CATEGORIES } from "../../collections/category";
import { Thumbs } from '../../collections/+api/image/collection';
import { USER_ORDERS } from "../../collections/userOrders";
import {min} from "rxjs/operator/min";
import {USER_BRANCHES} from "../../collections/userBranchs";
import {USER_TYPES} from "../../collections/usertypes";

Meteor.publish('MediaKitCategories',function(){
    var isLoggedIn = this.userId;
    let isAdmin: Boolean = RBAC.isAdmin(isLoggedIn);
    if(Roles.userIsInRole(isLoggedIn,'super-admin',Roles.GLOBAL_GROUP) || isAdmin){
        return MEDIA_KIT_CATEGORIES.find();
    }
    else{
        this.stop();
        return;
    }
})

Meteor.publish('mediaKitPackages',function(){
    var isLoggedIn = this.userId;
    let isAdmin: Boolean = RBAC.isAdmin(isLoggedIn);
    if(Roles.userIsInRole(isLoggedIn,'super-admin',Roles.GLOBAL_GROUP)) {
        return MEDIA_KIT_PACKAGES.find();
    }
    if(isAdmin){
        return MEDIA_KIT_PACKAGES.find({userId:isLoggedIn});
    }
    else{
        this.stop();
        return;
    }
})

Meteor.publishComposite('mediaKits',{
    find: function(){
        var isLoggedIn = this.userId;
        var user = Meteor.users.findOne({_id:isLoggedIn},{fields:{_id:1,profile:1}});
        var loggedInUserType = USER_TYPES.findOne({_id:user.profile.userType});
        let isAdmin: Boolean = RBAC.isAdmin(isLoggedIn);
        let groups = RBAC.getUserRoleAndGroup(isLoggedIn);
        let group = groups.group;
        let join = {
            transform: function(m){
                m.category = MEDIA_KIT_CATEGORIES.findOne({_id:m.categoryId},{fields:{_id:1,title:1}});
                m.image = Thumbs.findOne({'originalId':m.imageId},{fields:{originalId:1,url:1}});
                return m;
            }
        };
        if(Roles.userIsInRole(isLoggedIn,'super-admin',Roles.GLOBAL_GROUP)) {
            return MEDIA_KIT.find({isDeleted:false},join);
        }
        if(isAdmin){
            return MEDIA_KIT.find({userId:isLoggedIn,isDeleted:false},join);
        }
        if(isLoggedIn){
            return MEDIA_KIT.find({isDeleted:false,groups:{$in:[group]}},join);
        }
        else{
            this.stop();
            return;
        }

    }
});

Meteor.publishComposite('getUserOrders',{
    find: function (){
        var isLoggedIn = this.userId;
        return USER_ORDERS.find({},{
            transform: function(o){
                o.user = Meteor.users.findOne({_id:o.userId},{fields:{_id:1,emails:1,username:1,profile:1}});
                return o;
            }
        })
    }
})

//Meteor.publishComposite('getMyOrders',{
//    find: function () {
//        var isLoggedIn  = this.userId;
//        return USER_ORDERS.find({userId:isLoggedIn},{
//            transform: function(order){
//                for(var o in order.orders){
//                    let mId= order.orders[o].mediaKitId;
//                    order.orders[o].mediaKitDetails = MEDIA_KIT.findOne(
//                        {_id:mId},
//                        {fields:{_id:1,title:1,description:1,imageId:1,categoryId:1,sizeAndPrice:1}}
//                    );
//                    let catId = order.orders[o].mediaKitDetails.categoryId;
//                    let category = MEDIA_KIT_CATEGORIES.findOne(
//                        {_id:catId},
//                        {fields:{_id:1,title:1}}
//                    );
//                    order.orders[o].mediaKitDetails.categoryTitle = category.title;
//                }
//                return order;
//            }
//        })
//    }
//})

//Meteor.publish('getOrderDetailById',function(orderId:string){
//        var loggedInUser = this.userId;
//        var _order = USER_ORDERS.findOne({_id:orderId});
//        var transform = function(order){
//            for(var o in order.orders){
//                order.orders[o].mediaKitDetails = MEDIA_KIT.findOne(
//                    {_id:order.orders[o].mediaKitId},
//                    {fields:{_id:1,title:1,description:1,imageId:1,categoryId:1,sizeAndPrice:1}}
//                );
//                let image = Thumbs.findOne(
//                    {originalId:order.orders[o].mediaKitDetails.imageId},
//                    {fields:{_id:1,url:1}}
//                );
//                let category = MEDIA_KIT_CATEGORIES.findOne(
//                    {_id:order.orders[o].mediaKitDetails.categoryId},
//                    {fields:{_id:1,title:1}}
//                );
//                let branch;
//                if(order.orders[o].branchId !== 'download'){
//                    branch = USER_BRANCHES.findOne(
//                        {_id:order.orders[o].branchId},
//                        {fields: {_id:1,name:1,address:1,contact:1}}
//                    );
//                }
//                let vendor;
//                if(order.orders[o].vendorId !== 'any'){
//                    vendor = Meteor.users.findOne(
//                        {_id:order.orders[o].vendorId},
//                        {fields:{_id:1,username:1,profile:1,emails:1}}
//                    )
//                }
//                order.orders[o].mediaKitDetails.imageThumb = image.url;
//                order.orders[o].mediaKitDetails.categoryTitle = category.title;
//                order.orders[o].branchDetails = branch;
//                order.orders[o].vendorDetails = vendor;
//            }
//            let user = Meteor.users.findOne(
//                {_id:order.userId},
//                {fields:{_id:1,username:1,emails:1,profile:1}}
//            )
//            order.userDetails = user;
//            return order;
//        }
//    if(Roles.userIsInRole(loggedInUser,'super-admin',Roles.GLOBAL_GROUP) || Roles.userIsInRole(loggedInUser,'admin',_order.group) || _order.userId === loggedInUser){
//        var self =this;
//        var observer = USER_ORDERS.find({_id:orderId}).observe({
//            added: function(document){
//                self.added('userOrders',document._id,transform(document));
//            },
//            changed: function(newDocument){
//                self.changed('userOrders',newDocument._id,transform(newDocument));
//            },
//            removed: function(oldDocument){
//                self.removed('userOrders',oldDocument._id);
//            }
//        });
//        self.onStop(function () {
//            observer.stop();
//        });
//        self.ready();
//    } else {
//        this.stop();
//        return;
//    }
//
//
//});


Meteor.publish('getMyOrders',function(){

    var loggedInUser = this.userId;

    var transform = function(order){
        for(var o in order.orders){
            let mId= order.orders[o].mediaKitId;
            order.orders[o].mediaKitDetails = MEDIA_KIT.findOne(
                {_id:mId},
                {fields:{_id:1,title:1,description:1,imageId:1,categoryId:1,sizeAndPrice:1}}
            );
            let catId = order.orders[o].mediaKitDetails.categoryId;
            let category = MEDIA_KIT_CATEGORIES.findOne(
                {_id:catId},
                {fields:{_id:1,title:1}}
            );
            order.orders[o].mediaKitDetails.categoryTitle = category.title;
        }
        return order;
    }

    if(loggedInUser){
        var self = this;

        var observer = USER_ORDERS.find().observe({
            added: function (document) {
                self.added('userOrders', document._id, transform(document));
            },
            changed: function (newDocument) {
                self.changed('userOrders', newDocument._id, transform(newDocument));
            },
            removed: function (oldDocument) {
                self.removed('userOrders', oldDocument._id);
            }
        });
        self.onStop(function () {
            observer.stop();
        });
        self.ready();
    }
    else{
        this.stop();
        return;
    }

})