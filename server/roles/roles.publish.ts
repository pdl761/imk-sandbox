/**
 * Created by gurpinder on 5/15/16.
 */

Meteor.publish('roles',function(){
    //return Meteor.roles.find({});
    let isLoggedIn = this.userId;
    if(Roles.userIsInRole(isLoggedIn,'super-admin',Roles.GLOBAL_GROUP)){
        return Meteor.roles.find({name:{$ne:'super-admin'}});
    }
    if(isLoggedIn){
        return Meteor.roles.find({name:{$nin:['super-admin','admin']}});
    }
    else{
        this.stop();
        return;
    }
});