/**
 * Created by gurpinder on 5/15/16.
 */

import { GROUPS } from '../../collections/groups.ts';
import { Response } from '../response.ts';

Meteor.methods({
    'createRole': (role) => {
        let currentLoggedInUser = Meteor.userId();
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP)){
            let roleExists = Meteor.roles.findOne({name:role});
            if(roleExists){
               return Response.error('Role '+ role +' already exists');
            }
            Roles.createRole(role);
           return Response.success('Role created successfully');
        }
        else{
            return Response.error('Access denied');
        }
    },
    'deleteRole': (role) => {
        if(role === 'super-admin'){
           return Response.error('Ayo hoy!');
        }
        let currentLoggedInUser = Meteor.userId();
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP)) {


            let allGroups = GROUPS.find({}, {fields: {_id: 0, userId: 0}}).fetch();
            for (let i = 0; i < allGroups.length; i++) {
                //console.log(allGroups[i].name);
                let groupToSearch = "roles." + (allGroups[i].title).replace(/\./g, '_');
                let _query = {};
                _query[groupToSearch] = {$in: [role]};
                var roleInUse = Meteor.users.findOne(_query, {fields: {_id: 1}});
                if (roleInUse) {
                     return Response.error('Role is in use');
                }
            }
            Roles.deleteRole(role);
            return Response.success('Role deleted');
        }
        else{
            return Response.error('Access Denied');
        }
    },
});