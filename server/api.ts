/**
 * Created by d3v on 5/23/16.
 */

export var RestApi = new Restivus({
    version: 'v1',
    useDefaultAuth: true,
    prettyJson: true,
    apiPath: 'api/',
    enableCors: true
});