/**
 * Created by gurpinder on 4/9/16.
 */

Accounts.config({
    sendVerificationEmail: false,
    forbidClientAccountCreation : true
});
