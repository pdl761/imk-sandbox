/**
 * Created by gurpinder on 5/14/16.
 */

import { Response } from '../response';
import { Thumbs } from '../../collections/+api/image/collection';
import { RBAC } from './rbacChecks';
import { USER_TYPES } from '../../collections/usertypes';
import { USER_BRANCHES } from '../../collections/userBranchs';

Meteor.methods({
    'createNewUser':(user)=>{
        let currentLoggedInUser = Meteor.userId();
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP) || Roles.userIsInRole(currentLoggedInUser,'admin',user.groupRoles[0].group)){
            var _fimg = user.profilePicId;
            var thumb;
            if(_fimg){
               thumb  = Thumbs.findOne({originalId:_fimg});
            }
            else{
                thumb = {url:null};
                _fimg = null;
                user.profilePic = null;
            }
            var groupRoles = user.groupRoles;
            var getCategoryName = USER_TYPES.findOne({_id:user.type});
            if(!getCategoryName){return Response.error('Invalid Category');}
            let newUser = {
                username: user.username,
                email: user.email,
                password: user.pwd,
                profile: {
                    firstName: user.firstname,
                    lastName: user.lastname,
                    email: user.email,
                    phone: user.phone,
                    profilePic: user.profilePic,
                    profilePicId: _fimg,
                    profilePicThumb: thumb.url,
                    subscribedTo: user.subscribedTo,
                    isPublic: false,
                    userType: user.type,
                    userTypeTitle : getCategoryName.title,
                    officeBranch:[]
                },
                meta:{}
            };
            var typeSubscribed = Meteor.users.findOne({_id:currentLoggedInUser},{fields:{"profile.userType":1}});
            if(Roles.userIsInRole(currentLoggedInUser,'admin',groupRoles[0].group)){
                if(groupRoles[0].role == 'super-admin' || groupRoles[0].role == 'admin'){
                    return Response.error('Please contact <b>Super-Admin</b> to perform this action');
                }
            }
            if(typeSubscribed){
                typeSubscribed = typeSubscribed.profile.userType;
                if(!Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP) && typeSubscribed !== user.type){
                    return Response.error('Acces Denied');
                }
            }
            let userCreated = Accounts.createUser(newUser);
            Roles.addUsersToRoles(userCreated,groupRoles[0].role, groupRoles[0].group);
            //for(var gr in groupRoles){
            //    Roles.addUsersToRoles(userCreated,groupRoles[gr].role, groupRoles[gr].group);
            //}
            return Response.success("User created successfully");
        }
        else{
            return Response.error('Access denied');
        }
    },
    'getUserList': (user) => {
        let currentLoggedInUser = Meteor.userId();
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP)){
            let userList = Meteor.users.find({},{fields:{_id:1,username:1,emails:1}}).fetch();
            return Response.success(null,userList);
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'deleteUser': (userId) => {
        let currentLoggedInUser = Meteor.userId();
        var canDelete = RBAC.checkIfAdminOf(userId);
        let performDelete = function(id){
            Meteor.users.remove({_id:id});
            return Response.success('User Deleted');
        }
        if(!userId){
            return Response.error('User id required');
        }
        if(currentLoggedInUser === userId){
            return Response.error('Wanna die haa?');
        }
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP)){
            return performDelete(userId);
        }
        if(canDelete){
            return performDelete(userId);
        }
        else{
            return Response.error('Access Denied');
        }


    },
    'getUserDetail':(userId) => {
        if(!userId){
            return Response.error('User id required');
        }
        let currentLoggedInUser = Meteor.userId();
        var canView = RBAC.checkIfAdminOf(userId);
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP) || currentLoggedInUser === userId || canView){
            let user = Meteor.users.findOne({_id:userId},{fields:{_id:1,username:1,emails:1,roles:1,createdAt:1,profile:1}});
            return Response.success('success',user);
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'updateProfilePic':(userId,imageUrl,imageId) => {
        let performUpdate = (id) => {
            var _fimg = imageId;
            var thumb;
            if(_fimg){
                thumb  = Thumbs.findOne({originalId:_fimg});
            }
            Meteor.users.update(id,{
                $set:{
                    'profile.profilePic': imageUrl,
                    'profile.profilePicId': _fimg,
                    'profile.profilePicThumb': thumb.url,
                }
            })
            return Response.success('User Profile Pic Updated');
        }
        var currentLoggedInUser = Meteor.userId();
        var canUpdate = RBAC.checkIfAdminOf(userId);
        if(!userId){
            return Response.error('User Id is required');
        }
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP)) {
            return performUpdate(userId);
        }
        if(userId == currentLoggedInUser){
            return performUpdate(currentLoggedInUser);
        }
        if(canUpdate){
            return performUpdate(userId);
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'updateGeneralInfo': (userId,user) => {
        let performUpdate = (id) => {
            Meteor.users.update(id,{
                $set:{
                    'profile.firstName': user.firstname,
                    'profile.lastName': user.lastname,
                    'profile.phone': user.phone,
                }
            })
            return Response.success('User Profile Updated');
        }
        var currentLoggedInUser = Meteor.userId();
        let canUpdate = RBAC.checkIfAdminOf(userId);
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP)) {
            return performUpdate(userId);
        }
        if(userId === currentLoggedInUser){
            return performUpdate(currentLoggedInUser);
        }
        if(canUpdate){
            return performUpdate(userId);
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'updateProfile' : (userId,profile) => {
        let performUpdate = (id) =>{
            Meteor.users.update(id,{
                $set:{
                    'profile.licence': profile.licence,
                    'profile.jobTitle': profile.jobTitle,
                    'profile.address.office.streetAddress': profile.addressOffice.street,
                    'profile.address.office.city': profile.addressOffice.city,
                    'profile.address.office.state': profile.addressOffice.state,
                    'profile.address.office.zipcode': profile.addressOffice.zipCode,
                    'profile.address.residential.streetAddress': profile.addressResidential.street,
                    'profile.address.residential.city': profile.addressResidential.city,
                    'profile.address.residential.state': profile.addressResidential.state,
                    'profile.address.residential.zipcode': profile.addressResidential.zipCode,
                    'profile.contact.phoneOffice':profile.contact.phoneOffice,
                    'profile.contact.mobile':profile.contact.mobile,
                    'profile.contact.fax':profile.contact.fax,
                    'profile.bio':profile.bio,
                    'profile.socialLinks.facebook':profile.socialLinks.facebook,
                    'profile.socialLinks.googlePlus':profile.socialLinks.googlePlus,
                    'profile.socialLinks.linkedIn':profile.socialLinks.linkedIn,
                    'profile.socialLinks.pinterest':profile.socialLinks.pinterest,
                    'profile.socialLinks.twitter':profile.socialLinks.twitter,
                    'profile.socialLinks.youtube':profile.socialLinks.youtube,
                }
            })
            return Response.success('User Profile Updated');
        }
        var currentLoggedInUser = Meteor.userId();
        var canUpdate = RBAC.checkIfAdminOf(userId);
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP)) {
            return performUpdate(userId);
        }
        if(userId === currentLoggedInUser){
            return performUpdate(currentLoggedInUser);
        }
        if(canUpdate){
            return performUpdate(userId);
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'getUserRoles':(userId) => {
        var loggedinUser = Meteor.userId();
        var roles = RBAC.getRolesAndSubProducts(loggedinUser);
        return roles;
    },
    'createPublicApi':(userId) =>{
        var user = userId;
        var currentLoggedInUser = Meteor.userId();
        var performUpdate = (id) => {
            Meteor.users.update(id,{
                $set:{
                    'profile.isPublic': true
                }
            })
            var domain  = Meteor.absoluteUrl();
            var url = domain+"api/v1/user/"+user;
            return Response.success('User public api created successfully',url);
        }
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP)) {
            return  performUpdate(userId);
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'removePublicApi':(userId)=>{
        var user = userId;
        var currentLoggedInUser = Meteor.userId();
        var performUpdate = (id) => {
            Meteor.users.update(id,{
                $set:{
                    'profile.isPublic': false
                }
            })
            return Response.success('User public api removed');
        }
        if(Roles.userIsInRole(currentLoggedInUser,'super-admin',Roles.GLOBAL_GROUP)) {
            return  performUpdate(userId);
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'addBranch':(userId,branch) =>{
        let user = Meteor.userId();

        let addBranch = (userId:String) =>{
            branch.name = branch.name.trim();
            var groups = RBAC.getUserRoleAndGroup(userId);
            let _user = userId;
            let _branch = {
                userId: _user,
                name: branch.name,
                group: groups.group,
                address:{
                    street:branch.streetAddress,
                    city:branch.city,
                    state: branch.state,
                    zipCode: branch.zipCode,
                 },
                contact:{
                    phone:branch.phone,
                    fax: branch.fax,
                    email:branch.email
                },
                createdAt: new Date().valueOf()
            }

            let q = {userId: _user,name:branch.name};
            let branchExists = USER_BRANCHES.findOne(q);
            if(branchExists){
                return Response.error('Branch with same name already exists');
            } else {
                USER_BRANCHES.insert(_branch);
                return Response.success('Office branch/location added successfully');
            }
        }
        if(Roles.userIsInRole(user,'super-admin',Roles.GLOBAL_GROUP) || RBAC.isAdmin(user)){
            let u;
            if(!userId){
                u = user;
            } else {
                u = userId;
            }
            return addBranch(u);
        }
        else{
            return Response.error('Access Denied');
        }
    },
    'delBranchById':(branchId:String)=>{
        let user = Meteor.userId();
        let isOwner = USER_BRANCHES.findOne({_id:branchId,userId:user});
        if(Roles.userIsInRole(user,'super-admin',Roles.GLOBAL_GROUP) || isOwner){
            USER_BRANCHES.remove({_id:branchId});
            return Response.success('Branch removed successfully');
        } else {
            return Response.error('Access Denied');
        }
    },
    'getBranches':(userId:String)=>{

        if(!userId){
            return Response.error('User Id is required');
        }
        let user = Meteor.userId();
        if(Roles.userIsInRole(user,'super-admin',Roles.GLOBAL_GROUP) || RBAC.isAdmin(user)){
            var branches =  USER_BRANCHES.find({userId:userId}).fetch();
            return Response.success(null,branches);
        } else {
            return Response.error('Access Denied');
        }

    }
});