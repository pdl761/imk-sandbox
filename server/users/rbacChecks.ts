import {PRODUCTS} from "../../collections/products";
/**
 * Created by d3v on 5/27/16.
 */

export let RBAC = {
    // LU = Logged in User
    checkGroups:(userId)=>{

        // get current logged in user role in group
        var loggedInUser = Meteor.user();
        var loggedInUserRoleInGroup = loggedInUser.roles;
        var groupLU;
        var roleLU;
        for(var i in loggedInUserRoleInGroup){
            groupLU = i;
            roleLU = loggedInUserRoleInGroup[i];

        }
        groupLU = (groupLU).replace(/\_/g,'.');

        // get passed user role in group
        var user = userId;
        var userRoleInGroup = Meteor.users.findOne({_id:user},{fields:{roles:1}});
        userRoleInGroup = userRoleInGroup.roles;
        var group;
        var role;
        for(var i in userRoleInGroup){
            group = i;
            role = userRoleInGroup[i];

        }
        group = (group).replace(/\_/g,'.');

        return {user:{ group:group,role:role[0]},currentLoggedInUser:{ group:groupLU,role:roleLU[0]}};
    },
    checkIfAdminOf:(userId)=>{

        // get current logged in user role in group
        var loggedInUser = Meteor.user();
        var loggedInUserRoleInGroup = loggedInUser.roles;
        var groupLU;
        var roleLU;
        for(var i in loggedInUserRoleInGroup){
            groupLU = i;
            roleLU = loggedInUserRoleInGroup[i];

        }
        groupLU = (groupLU).replace(/\_/g,'.');

        // get passed user role in group
        var user = userId;
        var userRoleInGroup = Meteor.users.findOne({_id:user},{fields:{roles:1}});
        userRoleInGroup = userRoleInGroup.roles;
        var group;
        var role;
        for(var i in userRoleInGroup){
            group = i;
            role = userRoleInGroup[i];

        }
        group = (group).replace(/\_/g,'.');
        role = role[0];
        if(group !== groupLU){
            return false;
        }
        if(role === 'admin'){
            return false;
        }
        if(group === groupLU && role === 'user'){
            return true;
        }
        else{
            return false;
        }

    },
    getUserRoleAndGroup: (userId) =>{
        var user = userId;
        var userRoleInGroup = Meteor.users.findOne({_id:user},{fields:{roles:1}});
        userRoleInGroup = userRoleInGroup.roles;
        var group;
        var role;
        for(var i in userRoleInGroup){
            group = i;
            role = userRoleInGroup[i];
        }
        group = (group).replace(/\_/g,'.');
        return { group:group,role:role[0]};
    },
    getSubscribedProducts:(userId) =>{
        var user = Meteor.users.findOne({_id:userId},{fields:{"profile.subscribedTo":1}});
        var userProducts = user.profile.subscribedTo;
        var subProd = [];
        for(var u in userProducts){
            var q = {_id:userProducts[u]};
            var p = PRODUCTS.findOne(q);
            subProd.push(p.name)
        }
        return subProd;
    },
    getRolesAndSubProducts:(userId)=>{
        var user = userId;
        var userRoleInGroups = Meteor.users.findOne({_id:user},{fields:{roles:1,"profile.subscribedTo":1}});
        var userRoleInGroup = userRoleInGroups.roles;
        var group;
        var role;
        for(var i in userRoleInGroup){
            group = i;
            role = userRoleInGroup[i];
        }
        group = (group).replace(/\_/g,'.');

        var userProducts = userRoleInGroups.profile.subscribedTo;
        var subProd = {};
        for(var u in userProducts){
            var q = {_id:userProducts[u]};
            var p = PRODUCTS.findOne(q);
            var pname = p.name;
            subProd[pname] = true;
        }
        return { group:group,role:role[0],products:subProd};
    },
    isAdmin: (userId)=>{
        let user  = userId;
        var group = Roles.getGroupsForUser(user);
        group =  (group[0]).replace(/\_/g,'.');
        if(Roles.userIsInRole(user,'admin',group)){
            return true;
        } else {
            return false;
        }
    }
}