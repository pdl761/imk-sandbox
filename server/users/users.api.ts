/**
 * Created by d3v on 5/31/16.
 */

import { RestApi } from '../api';

RestApi.addRoute('user/:userId',{authRequired: false},{
    get: function(){
        let userId: string = this.urlParams.userId;
        let user = Meteor.users.findOne({_id:userId,"profile.isPublic":true},{fields:{profile:1}});
        if(user){
            user.profile.subscribedTo = [];
            return {success:true,data:user}
        }
        else{
            return {
                success: false, message: 'No user found'
            }
        }

    }
})