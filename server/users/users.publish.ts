/**
 * Created by gurpinder on 5/14/16.
 */

import { USER_TYPES } from '../../collections/usertypes';
import { USER_BRANCHES } from '../../collections/userBranchs';

Meteor.publish('imkUsers',function(){

    let isLoggedIn = this.userId;
    if(Roles.userIsInRole(isLoggedIn,'super-admin',Roles.GLOBAL_GROUP)){
        return Meteor.users.find({_id:{$ne:isLoggedIn}},{fields:{_id:1,username:1,emails:1,roles:1,createdAt:1,profile:1}});
    }
    if(isLoggedIn){
        let _groups = Roles.getGroupsForUser(isLoggedIn,'admin');
        var query = {};
        query["roles."+_groups[0]] = {$exists:true,$ne:null};
        return Meteor.users.find(query,
            {fields:{_id:1,username:1,emails:1,roles:1,createdAt:1,profile:1}});
    }
    else{
        this.stop();
        return;
    }
});

Meteor.publish('getUserTypes', function () {
    let isLoggedIn = this.userId;
    if(Roles.userIsInRole(isLoggedIn,'super-admin',Roles.GLOBAL_GROUP)){
        return USER_TYPES.find({});
    }
    if(isLoggedIn){
        var type = Meteor.users.findOne({_id:isLoggedIn},{fields:{"profile.userType":1}});
        if(type){
           type = type.profile.userType;
            return USER_TYPES.find({_id:type});
        }
        else{
            this.stop();
            return;
        }
    }
    else{
        this.stop();
        return;
    }
});


