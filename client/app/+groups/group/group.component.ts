/**
 * Created by gurpinder on 5/14/16.
 */

import 'reflect-metadata';
import { Component } from '@angular/core';
import { FormBuilder, ControlGroup, Validators, Control } from '@angular/common';
import { MeteorComponent } from 'angular2-meteor';
import { ImkAlertService } from "../../shared/imk-alert/imk-alert.component";

@Component({
    moduleId: module.id,
    selector: 'create-group',
    templateUrl: 'group.component.html',
    providers: [ImkAlertService]
})

export class GroupComponent extends MeteorComponent{
    groupForm: ControlGroup;
    constructor(private _alert: ImkAlertService){
        super();
        let fb = new FormBuilder();
        this.groupForm = fb.group({
            groupName : ['',Validators.required]
        });
    }
    createGroup(group){
        Meteor.call('createGroup',group.groupName,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    (this.groupForm.controls['groupName']).updateValue('');
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message);
                }
            }
        });
    }
}