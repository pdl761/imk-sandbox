/**
 * Created by gurpinder on 5/14/16.
 */

import 'reflect-metadata';
import { Component, NgZone } from '@angular/core';
import { MeteorComponent } from 'angular2-meteor';
import { GROUPS } from "../../../../collections/groups";
import { ImkAlertService } from "../../shared/imk-alert/imk-alert.component";
import { ImkConfirmBtnComponent } from "../../shared/imk-confirm-btn/imk-confirm-btn.component";

@Component({
    moduleId: module.id,
    selector: 'group-list',
    directives: [ImkConfirmBtnComponent],
    templateUrl: 'group-list.component.html'
})

export class GroupListComponent extends MeteorComponent{
    groupList : Mongo.Cursor;
    constructor(private _zone: NgZone,
                private _alert: ImkAlertService
    ){
        super();
        this.autorun(()=>{
            this.subscribe('groups',()=>{
                this.groupList = GROUPS.find();
                //console.log("heelooo");
            },true);
        });
    }
    deleteGroup(groupId){
        Meteor.call('deleteGroup',groupId,(err,result)=>{
            if(err){
                this._alert.error(err);
            }
            else{
                if(result.success == true){
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message)
                }
            }
        });
    }
}