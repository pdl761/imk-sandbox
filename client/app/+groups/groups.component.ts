/**
 * Created by gurpinder on 5/14/16.
 */

import 'reflect-metadata';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { GroupComponent } from "./group/group.component";
import { GroupListComponent } from "./group-list/group-list.component";
import { LoaderService } from '../shared/loader/loader.service';

@Component({
    moduleId: module.id,
    selector: 'groups',
    templateUrl: 'groups.component.html',
    directives: [GroupComponent,GroupListComponent]
})

export class GroupsComponent implements [OnInit, OnDestroy]{
    constructor(public loader: LoaderService){

    }
    ngOnInit(){
       this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
}