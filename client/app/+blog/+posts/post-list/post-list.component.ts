/**
 * Created by gurpinder on 5/15/16.
 */

import 'reflect-metadata';
import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { MeteorComponent } from 'angular2-meteor';
import { POSTS } from "../../../../../collections/posts";
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";
import { ImkConfirmBtnComponent } from "../../../shared/imk-confirm-btn/imk-confirm-btn.component";
import { LoaderService } from "../../../shared/loader/loader.service";

@Component({
    moduleId: module.id,
    selector: 'posts-list',
    templateUrl: 'post-list.component.html',
    directives: [ROUTER_DIRECTIVES, ImkConfirmBtnComponent],
    providers: [ImkAlertService]
})

export class PostListComponent extends MeteorComponent implements [OnInit,OnDestroy]{
    postList: Mongo.Cursor;
    constructor(private _ngZone: NgZone,
                private _alert: ImkAlertService,
                private _router: Router,
                public loader: LoaderService
    ){
        super();
        this.autorun(()=>{
            this.subscribe('posts', () => {
                this.postList = POSTS.find();
                //console.log(this.postList);
            },true);
        });
    }
    ngOnInit(){
        this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
    delPost(id){
        Meteor.call('deletePost',id,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message)
                }
            }
        });
    }
}