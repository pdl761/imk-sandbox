/**
 * Created by gurpinder on 5/15/16.
 */

import 'reflect-metadata';
import { Component,AfterViewInit,OnInit,OnDestroy } from '@angular/core';
import { FormBuilder, ControlGroup, Validators } from '@angular/common';
import { MeteorComponent } from 'angular2-meteor';
import { POSTS } from "../../../../../collections/posts";
import { GROUPS } from "../../../../../collections/groups";
import { CATEGORIES } from '../../../../../collections/category';
import { RouteSegment } from '@angular/router';
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";
import { CropAndUploadImageComponent } from "../../../+images/image/image.component";
import { UploadedImageListComponent } from "../../../+images/image-list/image-list.component";
import { LoaderService } from "../../../shared/loader/loader.service";

@Component({
    moduleId: module.id,
    selector: 'create-new-post',
    templateUrl: 'post.component.html',
    directives:[CropAndUploadImageComponent,UploadedImageListComponent],
    providers: [ImkAlertService]
})

export class PostComponent extends MeteorComponent implements  [OnInit,OnDestroy,AfterViewInit]{
    ngAfterViewInit(){
        $('#wysiwyg-post').summernote({
            height: 400,
            focus: true
        });
    }

    pageTitle: String = 'Create new post';
    btnTitle: String = 'Create post';
    newPostForm : ControlGroup;
    groupList : any;
    categoryList :Mongo.Cursor;
    selectedGroups = [];
    _tags = [];
    _post: any = [{
        _id: '',
        title:'',
        featuredImage:'/img/imageph.png',
        featuredImageId:'',
        body:'',
        tags:[],
        categoryId:'',
        userId:'',
        isPublished:false,
        allowComments:false,
        isFeatured:false,
        groups:[]
    }];
    constructor(private _params: RouteSegment,
                private _alert: ImkAlertService,
                public loader: LoaderService
    ){
        super();
        let fb =  new FormBuilder();

        this.newPostForm = fb.group({
            title: ['',Validators.required],
            featuredImage: [''],
            featuredImageId: [''],
            body: ['',Validators.required],
            tag:[''],
            tags: [this._tags,Validators.required],
            categoryId: ['',Validators.required],
            isPublished: [false,Validators.required],
            allowComments: [false,Validators.required],
            isFeatured:[false,Validators.required],
            groups: [this.selectedGroups,Validators.required]

        });
        this.autorun(()=>{
            let _postId = this._params.getParam('postId');
            if(_postId){
                this.pageTitle = 'Edit post';
                this.btnTitle = 'Update post';
                Meteor.call('getPost',_postId,(err,result)=>{

                    if(err){
                        console.log(err);
                    }
                    else{
                       // console.log(result);
                        this._post = result;
                            this._tags = this._post.tags;
                            this.selectedGroups = this._post.groups;
                            (this.newPostForm.controls['tags']).updateValue(this._tags);
                            (this.newPostForm.controls['groups']).updateValue(this.selectedGroups);
                            (this.newPostForm.controls['isPublished']).updateValue(this._post.isPublished);
                            (this.newPostForm.controls['allowComments']).updateValue(this._post.allowComments);
                            (this.newPostForm.controls['isFeatured']).updateValue(this._post.isFeatured);
                            $('#wysiwyg-post').summernote('reset');
                            $('#wysiwyg-post').summernote('code',this._post.body);
                    }
                })
                //this.subscribe('getPost', (_postIdd) => {
                //    this._post =  POSTS.find().fetch();
                //    this._tags = this._post.tags;
                //    this.selectedGroups = this._post.groups;
                //    (this.newPostForm.controls['tags']).updateValue(this._tags);
                //    (this.newPostForm.controls['groups']).updateValue(this.selectedGroups);
                //    (this.newPostForm.controls['isPublished']).updateValue(this._post.isPublished);
                //    (this.newPostForm.controls['allowComments']).updateValue(this._post.allowComments);
                //    (this.newPostForm.controls['isFeatured']).updateValue(this._post.isFeatured);
                //    $('#wysiwyg-post').summernote('reset');
                //    $('#wysiwyg-post').summernote('code',this._post.body);
                //},true);
            }
            let _user = Meteor.user();
            if(_user){
                if(Roles.userIsInRole(_user,'super-admin',Roles.GLOBAL_GROUP)){
                    this.subscribe('groups', () => {
                        this.groupList =  GROUPS.find();
                    },true);
                }
                else{
                    Meteor.call('getGroupsForUser',(err,result)=>{
                        if(err){
                            console.log(err);
                        }
                        else{
                            this.groupList = result;
                        }
                    })
                }
            }

            //this.subscribe('groups', () => {
            //    this.groupList =  GROUPS.find();
            //    console.log(this.groupList);
            //},true);
            this.subscribe('categories',() => {
                this.categoryList = CATEGORIES.find();
            },true);
        });

    }
    ngOnInit(){
        this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
    toggle(item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) list.splice(idx, 1);
        else list.push(item);
    }
    exists(item, list) {
        return list.indexOf(item) > -1;
    }
    addTag(tag){
        var _t = (tag).trim();
        if(_t){
            this._tags.push(tag);
            (this.newPostForm.controls['tag']).updateValue('');
        }
    }
    removeTag(tag,list){
        var idx = list.indexOf(tag);
        if (idx > -1) list.splice(idx, 1);
    }
    resetPostFields(){
        (this.newPostForm.controls['title']).updateValue(this._tags);
        (this.newPostForm.controls['featuredImage']).updateValue(this.selectedGroups);
        (this.newPostForm.controls['isPublished']).updateValue(false);
        (this.newPostForm.controls['allowComments']).updateValue(false);
        (this.newPostForm.controls['isFeatured']).updateValue(false);
        $('#wysiwyg-post').summernote('reset');
    }
    newPost(post){
        var _postBody = $('#wysiwyg-post').summernote('code');

        Meteor.call('createPost',_postBody,post,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message)
                }
            }
        });
    }
    updatePost(post){
        let postId = this._post._id;
        var _postBody = $('#wysiwyg-post').summernote('code');
        Meteor.call('updatePost',postId,_postBody,post,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message)
                }
            }
        });
    }
    post(post){
        if(this._params.getParam('postId')){
            this.updatePost(post);
        }
        else{
            this.newPost(post);
        }
    }
    updateImages(event){
        this._post.featuredImage = event.url;
        this._post.featuredImageId = event._id;
        //console.log(event);
    }
}