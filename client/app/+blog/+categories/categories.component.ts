/**
 * Created by gurpinder on 5/15/16.
 */

import 'reflect-metadata';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { CategoryComponent } from "./category/category.component";
import { CategoryListComponent } from "./category-list/category-list.component";
import { LoaderService } from "../../shared/loader/loader.service";


@Component({
    moduleId: module.id,
    selector: 'categories',
    templateUrl: 'categories.component.html',
    directives: [CategoryComponent, CategoryListComponent]
})

export class CategoriesComponent implements [OnInit,OnDestroy]{
    constructor(public loader: LoaderService){}
    ngOnInit(){
        this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
}