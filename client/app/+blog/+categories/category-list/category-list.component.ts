/**
 * Created by gurpinder on 5/15/16.
 */

import 'reflect-metadata';
import { Component,NgZone } from '@angular/core';
import { MeteorComponent } from 'angular2-meteor';
import { CATEGORIES } from "../../../../../collections/category";
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";
import { ImkConfirmBtnComponent } from "../../../shared/imk-confirm-btn/imk-confirm-btn.component";

@Component({
    moduleId: module.id,
    selector: 'category-list',
    templateUrl: 'category-list.component.html',
    directives: [ImkConfirmBtnComponent],
    providers: [ImkAlertService]
})

export class CategoryListComponent extends MeteorComponent{
    categoryList: Mongo.Cursor;
    constructor(private _ngZone: NgZone,
                private _alert: ImkAlertService
    ){
        super();
        this.autorun(()=>{
            this.subscribe('categories', () => {
                this.categoryList = CATEGORIES.find();
            },true);
        });

    }
    delCategory(id){
        Meteor.call('deleteCategory',id,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message)
                }
            }
        });
    }
}