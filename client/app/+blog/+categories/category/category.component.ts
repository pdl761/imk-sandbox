/**
 * Created by gurpinder on 5/15/16.
 */

import 'reflect-metadata';
import { Component } from '@angular/core';
import { MeteorComponent } from 'angular2-meteor';
import { FormBuilder, ControlGroup, Validators, Control } from '@angular/common';
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";
import {concat} from "rxjs/observable/concat";
import {undefined} from "meteor/check";

@Component({
    moduleId: module.id,
    selector: 'create-category',
    templateUrl: 'category.component.html',
    providers: [ImkAlertService]
})

export class CategoryComponent extends MeteorComponent{
    categoryForm : ControlGroup;
    constructor(private _alert: ImkAlertService){
        super();
        let fb = new FormBuilder();
        this.categoryForm = fb.group({
            categoryName: ['',Validators.required]
        });
    }
    createCategory(category): void{
        Meteor.call('createCategory',category.categoryName,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result === undefined){
                    this._alert.error('Error occurred while creating category');
                    return;
                }
                if(result.success === true){
                    (this.categoryForm.controls['categoryName']).updateValue('');
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message);
                }
            }
        });
    }
}