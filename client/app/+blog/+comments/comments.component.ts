/**
 * Created by gurpinder on 5/15/16.
 */

import 'reflect-metadata';
import { Component } from '@angular/core';
import { CommentComponent } from "./comment/comment.component";
import { CommentListComponent } from "./comment-list/comment-list.component";
import { LoaderService } from "../../shared/loader/loader.service";

@Component({
    moduleId: module.id,
    selector: 'comments',
    templateUrl: 'comments.component.html',
    directives: [CommentComponent,CommentListComponent]
})

export class CommentsComponent {
    constructor( public loader: LoaderService){}
    ngOnInit(){
        this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
}