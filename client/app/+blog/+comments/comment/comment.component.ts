/**
 * Created by gurpinder on 5/15/16.
 */

import 'reflect-metadata';
import { Component } from '@angular/core';
import { MeteorComponent } from 'angular2-meteor';
import { FormBuilder, ControlGroup, Validators, Control } from '@angular/common';
import { RouteSegment } from "@angular/router";
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";

@Component({
    moduleId: module.id,
    selector: 'comment',
    templateUrl: 'comment.component.html',
    providers: [ImkAlertService]
})

export class CommentComponent extends MeteorComponent{
    postCommentForm : ControlGroup;
    _postId : String;
    constructor(private _param: RouteSegment,
                private _alert: ImkAlertService
    ){
        super();
        this._postId = this._param.getParam('postId');
        let fb = new FormBuilder();
        this.postCommentForm = fb.group({
            userName: ['',Validators.required],
            userEmail: ['',Validators.required],
            comment: ['',Validators.required]
        });
    }
    postComment(comment): void{
        Meteor.call('postComment',this._postId,comment,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    (this.postCommentForm.controls['userName']).updateValue('');
                    (this.postCommentForm.controls['userEmail']).updateValue('');
                    (this.postCommentForm.controls['comment']).updateValue('');
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message);
                }
            }
        });
    }
}