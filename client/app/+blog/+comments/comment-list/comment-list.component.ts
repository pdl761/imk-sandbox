/**
 * Created by gurpinder on 5/15/16.
 */

import 'reflect-metadata';
import { Component, NgZone } from '@angular/core';
import { MeteorComponent } from 'angular2-meteor';
import { RouteSegment } from '@angular/router';
import { COMMENTS } from "../../../../../collections/comments";
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";
import { ImkConfirmBtnComponent } from "../../../shared/imk-confirm-btn/imk-confirm-btn.component";

@Component({
    moduleId: module.id,
    selector: 'comment-list',
    templateUrl: 'comment-list.component.html',
    directives: [ImkConfirmBtnComponent],
    providers: [ImkAlertService]
})

export class CommentListComponent extends MeteorComponent{
    commentList: Mongo.Cursor;
    _postId: String;
    constructor(
        private _ngZone: NgZone,
        private _param: RouteSegment,
        private _alert: ImkAlertService
    ){
        super();
        this._postId = this._param.getParam('postId');
        this.autorun(()=>{

            this.subscribe('getPostComments',()=>{
                this.commentList = COMMENTS.find({postId:this._postId});
            },true)
        });
    }
    delComment(commentId){
        Meteor.call('delComment',commentId,this._postId,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message)
                }
            }
        });
    }
    approveComment(commentId){
        Meteor.call('approveComment',commentId,this._postId,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message)
                }
            }
        });
    }
    unApproveComment(commentId){
        Meteor.call('unApproveComment',commentId,this._postId,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message)
                }
            }
        });
    }
}