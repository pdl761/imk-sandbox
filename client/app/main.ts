/**
 * Created by gurpinder on 5/13/16.
 */

import 'reflect-metadata';
import { provide } from '@angular/core';
import { ROUTER_PROVIDERS } from '@angular/router';
import { bootstrap } from 'angular2-meteor-auto-bootstrap';
import { APP_BASE_HREF, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { AppComponent } from './app.component';
import { ImkAlertService } from './shared/imk-alert/imk-alert.component';
import { LoaderService } from './shared/loader/loader.service';
import { IMKCartService } from "./shared/cart/cart.service";

bootstrap(AppComponent, [
    ImkAlertService,
    LoaderService,
    IMKCartService,
    ROUTER_PROVIDERS,
    provide(LocationStrategy,{useClass:PathLocationStrategy}),
    provide(APP_BASE_HREF,{useValue:'/'})
]);
