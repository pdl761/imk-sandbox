/**
 * Created by gurpinder on 5/13/16.
 */

import 'reflect-metadata';
import { Component , OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES, Routes, Router } from '@angular/router';
import { LoaderComponent } from './shared/loader/loader.component';
import { LoginComponent } from "./+auth/login/login.component";
import { DashboardComponent } from "./+dashboard/dashboard.component";

@Component({
    selector: 'app',
    template: `<a [routerLink]="['/']"></a>
               <loader></loader>
               <router-outlet></router-outlet>`,
    directives: [ROUTER_DIRECTIVES,LoaderComponent]
})
@Routes([
    { path: '/', component: LoginComponent },
    { path: '/dashboard', component: DashboardComponent }
])
export class AppComponent implements OnInit{
    constructor(){}
    ngOnInit(){

    }
}