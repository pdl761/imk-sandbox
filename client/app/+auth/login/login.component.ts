/**
 * Created by gurpinder on 5/14/16.
 */

import 'reflect-metadata';
import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, ControlGroup, Validators, Control } from '@angular/common';
import { FooterSmallComponent } from "../../shared/+footer/small/footer-small.component";
import { LoaderService } from "../../shared/loader/loader.service";

@Component({
    moduleId: module.id,
    selector: 'login',
    templateUrl: 'login.component.html',
    directives: [FooterSmallComponent]
})

export class LoginComponent implements [OnInit,OnDestroy]{
    loginForm : ControlGroup;
    message: String;
    constructor(
        private _router: Router,
        private _ngZone: NgZone,
        public loader: LoaderService
    ){
        let fb = new FormBuilder;
        this.loginForm = fb.group({
            email: ['',Validators.required],
            pwd: ['',Validators.required]
        })
    }
    ngOnInit(){
        this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
    login(cred){
        Meteor.loginWithPassword(cred.email,cred.pwd,(err)=>{
            if(err){
                console.log(err);
                this._ngZone.run(()=>{
                    this.message = err.reason;
                });
            }
            else{
                this._ngZone.run(()=>{
                    this._router.navigate(['/dashboard']);
                });
            }
        });
    }

}