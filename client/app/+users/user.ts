/**
 * Created by d3v on 5/27/16.
 */

export var User = {
    _id : "",
    createdAt: "",
    username: "",
    emails: [{
        address: ""
    }],
    profile:{
        licence: "",
        firstName :"",
        lastName : "",
        jobTitle: "",
        email : "",
        phone : "",
        profilePic : "",
        profilePicId : "",
        profilePicThumb : "",
        subscribedTo : [],
        address:{
            office:{
                city:"",
                state:"",
                streetAddress:"",
                zipcode:""
            },
            residential:{
                city:"",
                state:"",
                streetAddress:"",
                zipcode:""
            }
        },
        contact:{
            fax:"",
            mobile:"",
            phoneOffice:""

        },
        socialLinks:{
            facebook:"",
            googlePlus:"",
            linkedIn:"",
            pinterest:"",
            twitter:"",
            youtube:""
        }
    }
}