/**
 * Created by d3v on 5/18/16.
 */

import 'reflect-metadata';
import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { MeteorComponent } from 'angular2-meteor';
import { ROUTER_DIRECTIVES, RouteSegment } from '@angular/router';
import { LoaderService } from "../../shared/loader/loader.service";

@Component({
    moduleId: module.id,
    selector: 'user-detail',
    templateUrl: 'user-detail.component.html',
    directives: [ROUTER_DIRECTIVES]
})

export class UserDetailComponent extends  MeteorComponent implements [OnInit,OnDestroy]{
    userId: any;
    user: any;
    constructor(
        private route: RouteSegment,
        private ngZone: NgZone,
        public loader: LoaderService
    ){
        super();
        this.userId = this.route.getParam('userId');
        this.autorun(()=>{
            Meteor.call('getUserDetail',this.userId,(err,result)=>{
               if(err){
                   console.log(err);
               }
                else{
                   console.log(result);
                   this.ngZone.run(()=>{
                       this.user = result.data;
                   })
               }
            });
        });
    }
    ngOnInit(){
        this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
}