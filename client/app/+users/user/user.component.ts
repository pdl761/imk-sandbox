/**
 * Created by gurpinder on 5/14/16.
 */

import 'reflect-metadata';
import { Component, Input, NgZone, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, ControlGroup, Validators} from '@angular/common';
import { MeteorComponent } from 'angular2-meteor';
import { RouteSegment } from '@angular/router';
import { GROUPS } from "../../../../collections/groups";
import { PRODUCTS } from "../../../../collections/products";
import { USER_TYPES } from '../../../../collections/usertypes';
import { ImkAlertService } from "../../shared/imk-alert/imk-alert.component";
import { CropAndUploadImageComponent } from "../../+images/image/image.component";
import { UploadedImageListComponent } from "../../+images/image-list/image-list.component";
import { LoaderService } from '../../shared/loader/loader.service';

@Component({
    moduleId: module.id,
    selector: 'create-user',
    templateUrl: 'user.component.html',
    directives:[CropAndUploadImageComponent,UploadedImageListComponent],
    providers: [ImkAlertService]

})

export class UserComponent extends MeteorComponent implements [OnInit, OnDestroy]{
    createUserForm : ControlGroup;
    groupList : any;
    roleList :Mongo.Cursor;
    productList: Mongo.Cursor;
    userTypes: Mongo.Cursor;
    selectedProducts = [];
    defaultDP: String = "/img/noAvatar.png";
    userDP: String = this.defaultDP;
    userDPId : String;
    rbac = [];

    constructor(
        private _alert: ImkAlertService,
        private router: RouteSegment,
        private zone: NgZone,
        public loader: LoaderService
    ){
        super();

        let fb =  new FormBuilder();
        this.createUserForm = fb.group({
            firstname: ['',Validators.required],
            lastname: ['',Validators.required],
            username: ['',Validators.required],
            email: ['',Validators.required],
            pwd: ['',Validators.required],
            group: ['',Validators.required],
            role: ['',Validators.required],
            phone: ['',Validators.required],
            profilePic: [this.userDP],
            profilePicId: [this.userDPId],
            subscribedTo: [this.selectedProducts],
            groupRoles :[this.rbac],
            type: ['',Validators.required]
        });
        this.autorun(()=>{
            let _user = Meteor.userId();
            if(_user){
                if(Roles.userIsInRole(_user,'super-admin',Roles.GLOBAL_GROUP)){
                    this.subscribe('groups', () => {
                        this.groupList =  GROUPS.find();
                    },true);
                }
                else{
                    Meteor.call('getGroupsForUser',(err,result)=>{
                        if(err){
                            console.log(err);
                        }
                        else{
                            this.groupList = result;
                        }
                    })
                }
            }
            this.subscribe('roles',() => {
                this.roleList = Meteor.roles.find();
            },true);
            this.subscribe('products',() => {
                this.productList = PRODUCTS.find();
            },true);
            this.subscribe('getUserTypes',() => {
                this.userTypes = USER_TYPES.find();
            },true);
        });
    }
    ngOnInit(){
        this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
    createUser(user){
        console.log(user);
        Meteor.call('createNewUser',user,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    this._alert.success(result.message);
                    (this.createUserForm.controls['firstname']).updateValue('');
                    (this.createUserForm.controls['lastname']).updateValue('');
                    (this.createUserForm.controls['username']).updateValue('');
                    (this.createUserForm.controls['email']).updateValue('');
                    (this.createUserForm.controls['pwd']).updateValue('');
                    (this.createUserForm.controls['phone']).updateValue('');
                    this.zone.run(()=>{
                        this.userDP = this.defaultDP;
                    })


                }
                else{
                    this._alert.error(result.message);
                }
            }
        });
    }
    updateImages(event){
        this.userDP = event.url;
        this.userDPId = event._id;
    }
    toggle(item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) list.splice(idx, 1);
        else list.push(item);
    }
    exists(item, list) {
        return list.indexOf(item) > -1;
    }
    addRG(role,group){
        var rbac = this.rbac;
        for(var rb in rbac){
            if(rbac[rb].group == group){
                this._alert.error('Role already assigned in group <b>'+ group +'</b>')
                return;
            }
        }
        if(rbac.length > 0){
            this._alert.error('Role in group already defined');
            return;
        }
        this.rbac.push({group:group,role:role});
    }
    removeRG(rg){
        var rbac = this.rbac,
        del = rbac.indexOf(rg);
        rbac.splice(del,1);
    }
}