/**
 * Created by d3v on 5/25/16.
 */

import 'reflect-metadata';
import { Component, Input, NgZone, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, ControlGroup, Validators} from '@angular/common';
import { RouteSegment } from '@angular/router';
import { MeteorComponent } from 'angular2-meteor';
import { CropAndUploadImageComponent } from "../../../+images/image/image.component";
import { UploadedImageListComponent } from "../../../+images/image-list/image-list.component";
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";
import { LoaderService } from "../../../shared/loader/loader.service";

@Component({
    moduleId: module.id,
    selector: 'user-edit-profile-pic',
    templateUrl: 'edit-profile-pic.component.html',
    directives: [CropAndUploadImageComponent,UploadedImageListComponent],
    providers: [ImkAlertService]
})

export class UserEditProfilePicComponent extends MeteorComponent implements [OnInit, OnDestroy]{
    defaultDP: string =  '/img/avatar';
    userDP: string = this.defaultDP;
    userDPId: string;
    user: any;
    userId: string;
    constructor(
        private _alert: ImkAlertService,
        private _param: RouteSegment,
        private _zone: NgZone,
        private loader: LoaderService
    ){
        super();
        this._zone.run(()=>{
            this.userId = this._param.getParam('userId');
           Meteor.call('getUserDetail',this.userId, (err,result) => {
                if(err){
                    console.log(err);
                }
               else{
                    this.user = result.data;
                   // console.log(result);
                    this.userDP = this.user.profile.profilePic;
                    this.userDPId = this.user.profile.profilePicId;
                }
            });
        })


    }
    ngOnInit(){
        this.loader.stop()
    }
    ngOnDestroy(){
        this.loader.start();
    }
    updateDP(){
        //console.log(this.userDP,this.userDPId);
        Meteor.call('updateProfilePic',this.userId,this.userDP,this.userDPId,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message);
                }

            }
        })
    }
    updateImages(event){
        this.userDP = event.url;
        this.userDPId = event._id;
    }
}
