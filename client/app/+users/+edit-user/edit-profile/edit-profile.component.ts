/**
 * Created by d3v on 5/26/16.
 */

import 'reflect-metadata';
import { Component, NgZone, AfterViewInit, OnInit, OnDestroy } from '@angular/core';
import { RouteSegment } from '@angular/router';
import { MeteorComponent } from 'angular2-meteor';
import { FormBuilder, ControlGroup, Validators} from '@angular/common';
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";
import { User } from '../../user';
import { LoaderService } from "../../../shared/loader/loader.service";

@Component({
    moduleId: module.id,
    selector: 'user-edit-profile',
    templateUrl: 'edit-profile.component.html',
    providers: [ImkAlertService]
})

export class UserEditProfileComponent extends MeteorComponent implements [AfterViewInit, OnInit, OnDestroy]{
    ngAfterViewInit(){
        $('#wysiwyg-userProfile').summernote({
            height: 400,
            focus: false
        });
    }
    user: any = User;
    userId: string;
    editProfile: ControlGroup;
    constructor(
        private _zone: NgZone,
        private _param: RouteSegment,
        private _alert: ImkAlertService,
        private loader: LoaderService
    ){
        super()
        let fb = new FormBuilder();
        this.editProfile = fb.group({
            licence: [''],
            jobTitle: [''],
            addressOffice:fb.group({
                street: [''],
                city: [''],
                state: [''],
                zipCode: [''],
            }),
            addressResidential:fb.group({
                street: [''],
                city: [''],
                state: [''],
                zipCode: [''],
            }),
            contact: fb.group({
                phoneOffice: [''],
                mobile: [''],
                fax: [''],
            }),
            socialLinks:fb.group({
                facebook: [''],
                twitter:[''],
                linkedIn:[''],
                googlePlus:[''],
                youtube:[''],
                pinterest:['']
            })

        })

            this.userId = this._param.getParam('userId');
            Meteor.call('getUserDetail',this.userId, (err,result) => {
                if(err){
                    console.log(err);
                }
                else{
                    this._zone.run(()=>{
                        var _user = $.extend(true,this.user,result.data);
                        this.user = _user;
                        $('#wysiwyg-userProfile').summernote('reset');
                        $('#wysiwyg-userProfile').summernote('code',this.user.profile.bio);
                    })
                    console.log(this.user);
                }
            });

    }
    ngOnInit(){
        this.loader.stop()
    }
    ngOnDestroy(){
        this.loader.start();
    }
    updateProfile(profile){
        var bio = $('#wysiwyg-userProfile').summernote('code');
        profile.bio = bio;
        console.log(profile);

        Meteor.call('updateProfile',this.userId,profile,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message);
                }
            }
        })
    }
}