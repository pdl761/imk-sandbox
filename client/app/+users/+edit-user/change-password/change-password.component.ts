/**
 * Created by d3v on 5/26/16.
 */

import 'reflect-metadata';
import { Component, OnInit, OnDestroy} from '@angular/core';
import { Accounts } from 'meteor/accounts-base';
import { FormBuilder, ControlGroup, Validators} from '@angular/common';
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";
import { LoaderService } from "../../../shared/loader/loader.service";

@Component({
    moduleId: module.id,
    selector: 'user-change-password',
    templateUrl: 'change-password.component.html',
    providers: [ImkAlertService]
})

export class UserChangePasswordComponent implements [OnInit, OnDestroy]{
    userCP : ControlGroup;
    constructor(
        private _alert: ImkAlertService,
        private loader: LoaderService
    ){
        let fb = new FormBuilder();
        this.userCP = fb.group({
            oldPwd: ['',Validators.required],
            newPwd: ['',Validators.required],
            cNewPwd: ['',Validators.required]
        })
    }
    ngOnInit(){
        this.loader.stop()
    }
    ngOnDestroy(){
        this.loader.start();
    }
    changePassword(pass){
        Accounts.changePassword(pass.oldPwd,pass.newPwd,(err)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
               this._alert.success("Password updated successfully");
                (this.userCP.controls['oldPwd']).updateValue('');
                (this.userCP.controls['newPwd']).updateValue('');
                (this.userCP.controls['cNewPwd']).updateValue('');
            }
        })
    }
}