/**
 * Created by d3v on 5/26/16.
 */

import 'reflect-metadata';
import { Component, NgZone } from '@angular/core';
import { RouteSegment } from '@angular/router';
import { MeteorComponent } from 'angular2-meteor';
import { FormBuilder, ControlGroup, Validators} from '@angular/common';
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";

@Component({
    moduleId: module.id,
    selector: 'user-manage-roles-groups',
    templateUrl: 'manage-roles-groups.component.html',
    providers: [ImkAlertService]
})

export class UserManageRolesComponent extends MeteorComponent{
    user: any;
    roles: any = {};
    userId: string;
    constructor(
        private _zone: NgZone,
        private _param: RouteSegment,
        private _alert: ImkAlertService
    ){
        super();
        this.userId = this._param.getParam('userId');
        Meteor.call('getUserDetail',this.userId, (err,result) => {
            if(err){
                console.log(err);
            }
            else{
                var d =  result.data.roles;
                for(var r in d){
                    console.log(r,r[0]);
                }
                this._zone.run(()=>{

                })
                console.log(this.user);
            }
        });
    }
}