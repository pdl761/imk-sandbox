/**
 * Created by gurpinder on 5/22/16.
 */

import 'reflect-metadata';
import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { Router, ROUTER_DIRECTIVES, Routes, RouteSegment } from '@angular/router';
import { UserEditGeneralInfoComponent } from "./edit-general-info/edit-general-info.component";
import { UserEditProfileComponent } from "./edit-profile/edit-profile.component";
import { UserManageRolesComponent } from "./manage-roles-groups/manage-roles-groups.component";
import { UserManageProductSubscriptionComponent } from "./manage-product-subscription/manage-product-subscription.component";
import { LoaderService } from "../../shared/loader/loader.service";
import { UserLocationListComponent } from "../+locations/location-list/location-list.component";
import { UserEditProfilePicComponent } from "./edit-profile-pic/edit-profile-pic.component";
import { UserChangePasswordComponent } from "./change-password/change-password.component";
import { UserNewLocationComponent } from "../+locations/location/location.component";

@Component({
    moduleId: module.id,
    selector: 'edit-user',
    templateUrl: 'edit-user.component.html',
    directives:[ROUTER_DIRECTIVES]
})
@Routes([
    { path: '/change-profile-pic/:userId', component: UserEditProfilePicComponent },
    { path: '/general-info/:userId', component: UserEditGeneralInfoComponent },
    { path: '/edit-profile/:userId',component: UserEditProfileComponent },
    { path: '/change-password/:userId', component: UserChangePasswordComponent },
    { path: '/add-branch/:userId', component: UserNewLocationComponent },
    { path: '/branch-list/:userId', component: UserLocationListComponent },
    { path: '/manage-product-subscription/:userId', component: UserManageProductSubscriptionComponent }
])
export class EditUserComponent implements [OnInit,OnDestroy] {
    userId: String;
    userRoles:any = {
        group:"",
        products:"",
        role:""
    };
    isSU: any;
    constructor(
        public loader: LoaderService,
        public _router : Router,
        public _param: RouteSegment,
        private _zone: NgZone
    ){
        this.userId = this._param.getParam('userId');
        Meteor.call('getUserRoles',(err,result)=>{
            if(err){}
            else {
                //console.log(result);
                this._zone.run(()=> {
                    this.userRoles = result;
                    this.isSU = Roles.userIsInRole(Meteor.userId(),'super-admin',Roles.GLOBAL_GROUP);
                });
            }
        })
    }
    ngOnInit(){
        this.loader.stop();
        this._router.navigate(['/dashboard/edit-user/'+this.userId+'/change-profile-pic',this.userId]);
    }

    ngOnDestroy(){
        this.loader.start();
    }
}