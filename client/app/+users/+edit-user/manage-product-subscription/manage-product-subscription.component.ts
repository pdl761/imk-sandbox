/**
 * Created by gurpinder on 5/29/16.
 */

import 'reflect-metadata';
import { Component, NgZone, OnInit, OnDestroy} from '@angular/core';
import { MeteorComponent } from "angular2-meteor/build/index";
import { PRODUCTS } from "../../../../../collections/products";
import { RouteSegment } from '@angular/router';
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";
import { LoaderService } from "../../../shared/loader/loader.service";

@Component({
    moduleId: module.id,
    selector: 'user-manage-product-subscription',
    templateUrl: 'manage-product-subscription.component.html',
    providers: [ImkAlertService]
})

export class UserManageProductSubscriptionComponent extends MeteorComponent implements [OnInit, OnDestroy]{
    orgProducts: Mongo.Cursor;
    subscribedTo: any;
    userId: string;
    userPublicApiUrl: string = "";
    constructor(
        private _zone: NgZone,
        private _param: RouteSegment,
        private _alert : ImkAlertService,
        private loader: LoaderService
    ){
        super();
        this.userId = this._param.getParam('userId');

        this.autorun(()=>{
            this.subscribe('products',()=>{
                this.orgProducts = PRODUCTS.find();
            },true);
        })
        this.getAccessedProd(this.userId);
    }
    ngOnInit(){
        this.loader.stop()
    }
    ngOnDestroy(){
        this.loader.start();
    }
    getAccessedProd(uid){
        console.log(uid,"d");
        Meteor.call('getAllowedProducts',uid,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                console.log(result);
                this._zone.run(()=>{
                    this.subscribedTo = result;
                })
            }
        })
    }
    allowAccess(product){
        Meteor.call('allowProductAccess',this.userId,product,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success === true){
                    this._alert.success(result.message);
                    this._zone.run(()=>{
                        this.subscribedTo.push(product);
                    })
                }
                else{
                    this._alert.error(result.message);
                }

            }
        })
    }
    removeAccess(product){
        Meteor.call('denyProductAccess',this.userId,product,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success === true){
                    this._alert.success(result.message);
                    this._zone.run(()=>{
                        this.subscribedTo.splice(this.subscribedTo.indexOf(product),1);
                    })
                }
                else{
                    this._alert.error(result.message);
                }
            }
        })
    }
    createPublicApi(){
        Meteor.call('createPublicApi',this.userId,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success === true){
                    this._zone.run(()=>{
                        this.userPublicApiUrl = result.data;
                    })
                    this._alert.success(result.message);
                }
                else {
                    this._alert.error(result.message);
                }
            }
        })
    }
    removePublicApi(){
        Meteor.call('removePublicApi',this.userId,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success === true){
                    this._zone.run(()=> {
                        this.userPublicApiUrl = "";
                    })
                    this._alert.success(result.message);

                }
                else {
                    this._alert.error(result.message);
                }
            }
        })
    }
}