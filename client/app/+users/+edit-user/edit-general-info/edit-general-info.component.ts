/**
 * Created by d3v on 5/25/16.
 */

import 'reflect-metadata';
import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { RouteSegment } from '@angular/router';
import { FormBuilder, ControlGroup, Validators} from '@angular/common';
import { MeteorComponent } from "angular2-meteor/build/index";
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";
import { User } from '../../user';
import { LoaderService } from "../../../shared/loader/loader.service";

@Component({
    moduleId: module.id,
    selector: 'user-edit-general-info',
    templateUrl: 'edit-general-info.component.html',
    providers: [ImkAlertService]
})

export class UserEditGeneralInfoComponent extends MeteorComponent implements  [OnInit, OnDestroy]{
    editUserInfo: ControlGroup;
    userId: string;
    user: any = User;
    constructor(
        private _zone: NgZone,
        private _param: RouteSegment,
        private _alert: ImkAlertService,
        private loader: LoaderService
    ){
        let fb =  new FormBuilder();
        this.editUserInfo = fb.group({
            firstname: ['',Validators.required],
            lastname: ['',Validators.required],
            phone: ['',Validators.required],
        });
        super();
        this._zone.run(()=>{
            this.userId = this._param.getParam('userId');
            Meteor.call('getUserDetail',this.userId, (err,result) => {
                if(err){
                    console.log(err);
                }
                else{
                    var _user = $.extend(true,this.user,result.data);
                    console.log(_user);
                    this.user = _user;
                    //console.log(this.user);
                }
            });
        })
    }
    ngOnInit(){
        this.loader.stop()
    }
    ngOnDestroy(){
        this.loader.start();
    }
    updateGInfo(user){
        Meteor.call('updateGeneralInfo',this.userId,user,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message);
                }
            }
        })
    }
}
