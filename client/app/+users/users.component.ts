/**
 * Created by gurpinder on 5/14/16.
 */

import 'reflect-metadata';
import { Component } from '@angular/core';
import { UserComponent } from "./user/user.component";
import { UserListComponent } from "./user-list/user-list.component";

@Component({
    moduleId: module.id,
    selector: 'users',
    templateUrl: 'users.component.html',
    directives: [UserComponent,UserListComponent]
})

export class UsersComponent {}