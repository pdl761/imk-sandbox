/**
 * Created by d3v on 6/6/16.
 */

import 'reflect-metadata';
import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { LoaderService } from "../../../shared/loader/loader.service";
import { MeteorComponent } from "angular2-meteor/build/index";
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";
import { ImkConfirmBtnComponent } from "../../../shared/imk-confirm-btn/imk-confirm-btn.component";
import { RouteSegment } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'user-location-list',
    templateUrl: 'location-list.component.html',
    directives: [ ImkConfirmBtnComponent ]
})

export class UserLocationListComponent extends MeteorComponent implements [OnInit, OnDestroy]{
    branchList: any;
    userId: String;
    constructor(
        private loader: LoaderService,
        private _zone: NgZone,
        private _alert: ImkAlertService,
        private _param: RouteSegment
    ){
        super();
        this.userId = this._param.getParam('userId');
        this.getBranches();
    }
    ngOnInit(){
        this.loader.stop()
    }
    ngOnDestroy(){
        this.loader.start();
    }
    getBranches(){
        Meteor.call('getBranches',this.userId,(err,result)=>{
            if(err) {
                console.log(err.reason);
            }
            else{
                console.log(result);
                if(result.success === true){
                    this._zone.run(()=>{
                        this.branchList = result.data;
                    })
                }
            }
        })
    }
    delBranch(branch){
        Meteor.call('delBranchById',branch._id,(err,result)=>{
            if(err) {
                console.log(err.reason);
            }
            else{
                if(result.success === true){
                    this._alert.success(result.message);
                    this._zone.run(()=>{
                        this.branchList.splice(this.branchList.indexOf(branch),1);
                    })
                } else {
                    this._alert.error(result.message);
                }

            }
        })
    }
}