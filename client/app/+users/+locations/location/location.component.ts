/**
 * Created by d3v on 6/6/16.
 */

import 'reflect-metadata';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, ControlGroup, Validators, Control } from '@angular/common';
import { LoaderService } from "../../../shared/loader/loader.service";
import { MeteorComponent } from "angular2-meteor/build/index";
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";
import { RouteSegment } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'new-user-location',
    templateUrl: 'location.component.html'
})

export class UserNewLocationComponent extends MeteorComponent implements [OnInit, OnDestroy]{
    newBranchForm: ControlGroup;
    userId: String;
    constructor(
        private loader: LoaderService,
        private _alert: ImkAlertService,
        private _param: RouteSegment
    ){

        let fb = new FormBuilder();
        this.userId = this._param.getParam('userId');
        this.newBranchForm = fb.group({
            name: ['',Validators.required],
            streetAddress: ['',Validators.required],
            city: ['',Validators.required],
            state: ['',Validators.required],
            zipCode: ['',Validators.required],
            phone: ['',Validators.required],
            fax:['',Validators.required],
            email:[]
        })
    }
    ngOnInit(){
        this.loader.stop()
    }
    ngOnDestroy(){
        this.loader.start();
    }
    addBranch(branch){
        console.log(this.userId,branch);
        Meteor.call('addBranch',this.userId,branch,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success === true){
                    this._alert.success(result.message);
                    (this.newBranchForm.controls['name']).updateValue('');
                    (this.newBranchForm.controls['streetAddress']).updateValue('');
                    (this.newBranchForm.controls['city']).updateValue('');
                    (this.newBranchForm.controls['state']).updateValue('');
                    (this.newBranchForm.controls['zipCode']).updateValue('');
                    (this.newBranchForm.controls['phone']).updateValue('');
                    (this.newBranchForm.controls['fax']).updateValue('');
                    (this.newBranchForm.controls['email']).updateValue('');
                }
                else{
                    this._alert.error(result.message);
                }
            }
        })
    }
}