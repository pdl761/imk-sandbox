/**
 * Created by gurpinder on 5/14/16.
 */

import 'reflect-metadata';
import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { MeteorComponent } from 'angular2-meteor';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { ImkAlertService } from "../../shared/imk-alert/imk-alert.component";
import { ImkConfirmBtnComponent } from "../../shared/imk-confirm-btn/imk-confirm-btn.component";
import { LoaderService } from "../../shared/loader/loader.service";

@Component({
    moduleId: module.id,
    selector: 'user-list',
    templateUrl: 'user-list.component.html',
    directives: [ROUTER_DIRECTIVES, ImkConfirmBtnComponent],
    providers: [ImkAlertService]
})

export class UserListComponent extends MeteorComponent implements [OnInit,OnDestroy]{
    userList : Mongo.Cursor;
    constructor(private _zone: NgZone,
                private _alert: ImkAlertService,
                public loader: LoaderService
    ){
        super();
        this.autorun(()=>{
            this.subscribe('imkUsers',()=>{
                this.userList = Meteor.users.find();
                //console.log(this._users);
            },true);
        });


    }
    ngOnInit(){
        this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
    delUser(id){
        console.log(id);
        Meteor.call('deleteUser',id,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    this._alert.success(result.message);
                    console.log(result);
                }
                else{
                    this._alert.error(result.message);
                    console.log(result);
                }
            }
        });
    }

}