/**
 * Created by d3v on 6/1/16.
 */

import 'reflect-metadata';
import { Injectable } from '@angular/core';

@Injectable()
export class IMKCartService {
   cartItems: any = [];

    constructor(){

    }
    getData(){
        return this.cartItems;
    }
    setData(data){
        var cartList = this.cartItems;
        for(var item in cartList){
            if(cartList[item].itemId === data.itemId){
                this.cartItems.splice(this.cartItems.indexOf(cartList[item]),1);
            }
        }
        data.editItem = false;
        var d = this.cartItems.push(data);
        if(d){
            return {success: true}
        }
        else{
            return  {success: false, reason: 'error'}
        }
    }
    removeItem(item){
        this.cartItems.splice(this.cartItems.indexOf(item),1);
    }
    resetCart(){
        this.cartItems  = [];
    }
}
