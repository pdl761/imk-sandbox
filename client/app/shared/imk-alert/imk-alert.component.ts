/**
 * Created by gurpinder on 5/15/16.
 */

export class ImkAlertService {
    data: any;
    constructor(){}
    error(err){
        $("#imk-alert").show();
        $('<li class="alert alert-danger">'+err+'<span class="pull-right" onClick="$(this).parent().remove()">&times;</span></li>')
            .prependTo("#imk-alert ul").hide().slideDown({duration:100})
            .delay(5000).queue(function(){
            $(this).remove();
        });
    }
    success(message){
        //alert(message);
        $("#imk-alert").show();
        $('<li class="alert alert-success">'+message+'<span class="pull-right" onClick="$(this).parent().remove()">&times;</span></li>')
            .prependTo("#imk-alert ul").hide().slideDown({duration:100})
            .delay(3000).queue(function(){
                $(this).fadeOut(4000).remove();
        });
    }
}