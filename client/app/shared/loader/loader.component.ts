/**
 * Created by d3v on 5/30/16.
 */

import { Component } from '@angular/core';
import { LoaderService } from './loader.service';
@Component({
    selector: 'loader',
    template: '<div *ngIf="active"><h4 class="_loader">Loading..</h4></div>',
    styles:[`
        ._loader{
            height: 50px;
            background: #ff7878;
            width: 100%;
            text-align: center;
            color: #fff;
            position: fixed;
            top: 40%;
            text-transform: uppercase;
            letter-spacing: 20px;
            padding: 12px;
            z-index:99999;
        }
    `]
})
export class LoaderComponent {
    public active: boolean = true;

    public constructor(loader: LoaderService) {
        loader.status.subscribe((status: boolean) => {
            this.active = status;
        });
    }
}