/**
 * Created by d3v on 6/2/16.
 */

import 'reflect-metadata';
import { Injectable } from '@angular/core';
import { MeteorComponent } from "angular2-meteor/build/index";

@Injectable()
export class UserService extends MeteorComponent{
    userProfile: any;
    constructor(){
        super();


    }
    getUserProfile(){
        this.autorun(()=>{
            this.userProfile = Meteor.user();
            return this.userProfile;
        });
        //Meteor.call('getUserRoles',(err,result)=>{
        //    if(err){}
        //    else {
        //        this.userProfile.userRoles = result;
        //        this.userProfile.isSU = Roles.userIsInRole(Meteor.userId(),'super-admin',Roles.GLOBAL_GROUP);
        //    }
        //})

    }
}