/**
 * Created by d3v on 5/20/16.
 */

import 'reflect-metadata';
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'imk-confirm-btn',
    templateUrl :'imk-confirm-btn.component.html',
    styleUrls: ['imk-confirm-btn.component.css']
})

export class ImkConfirmBtnComponent implements OnInit{
    @Input() title : string = "Confirm action, continue?";
    @Input() ok : string = "Yes";
    @Input() cancel :string = "No";
    @Input() btnType: string = "btn-danger";
    @Input() btnName: string = '<i class="fa fa-trash"></i>';

    @Output() okFunc =  new EventEmitter<string>();
    @Output() cancelFunc =  new EventEmitter<string>();
    confirmBtn : any = {
        title: "",
        yes: "",
        no: "",
        btnType: "",
        btnName:"",
    }
    ngOnInit(){
        this.confirmBtn.title = this.title;
        this.confirmBtn.yes = this.ok;
        this.confirmBtn.no = this.cancel;
        this.confirmBtn.btnType = this.btnType;
        this.confirmBtn.btnName = this.btnName;
    }
    constructor(){}

    callOk(){
        this.okFunc.emit(null);
    }
    callCancel(){
        this.cancelFunc.emit(null);
    }
}
    // HOW TO USE EXAMPE
     //  <imk-confirm-btn
     // [title]="'Delete user?'"
     // [ok]="'Yes'"
     // [cancel]="'Cancel'"
     // (okFunc)="func()"
    //  (cancelFunc)="func()"
     //></imk-confirm-btn>

