/**
 * Created by gurpinder on 5/14/16.
 */

import 'reflect-metadata';
import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'footer-small',
    templateUrl: 'footer-small.component.html'
})

export class FooterSmallComponent {}