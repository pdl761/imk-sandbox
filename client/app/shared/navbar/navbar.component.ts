/**
 * Created by gurpinder on 5/14/16.
 */

import 'reflect-metadata';
import { Component, NgZone } from '@angular/core';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { MeteorComponent } from "angular2-meteor";

@Component({
    moduleId: module.id,
    selector: 'navbar-top',
    templateUrl: 'navbar.component.html',
    directives: [ROUTER_DIRECTIVES]
})

export class NavbarTopComponent extends MeteorComponent{
    loggedInUser: any ;
    isSU: Boolean = false;
    userRoles:any = {
        group:"",
        products:"",
        role:""
    };
    constructor(
        private _ngZone: NgZone,
        private _router: Router){
        super()
        this.autorun(()=>{
            let id = Meteor.userId();
            this.subscribe('imkUsers', () => {
                this.loggedInUser =  Meteor.users.find({_id:id}).fetch();
            },true);
            Meteor.call('getUserRoles',(err,result)=>{
                if(err){}
                else {

                    this._ngZone.run(()=> {
                        this.userRoles = result;
                        console.log(this.userRoles);
                        this.isSU = Roles.userIsInRole(Meteor.userId(),'super-admin',Roles.GLOBAL_GROUP);
                    });
                }
            })
        })
    }
    logOut(){
        Meteor.logout((err)=>{
            if(err){
                console.log(err);
            }
            else{
                this._router.navigate(['/']);
            }
        });
    }
}