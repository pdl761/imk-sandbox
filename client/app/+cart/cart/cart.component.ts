/**
 * Created by d3v on 6/1/16.
 */

import 'reflect-metadata';
import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { LoaderService } from "../../shared/loader/loader.service";
import { IMKCartService } from "../../shared/cart/cart.service";
import { ImkConfirmBtnComponent } from "../../shared/imk-confirm-btn/imk-confirm-btn.component";
import { ImkAlertService } from "../../shared/imk-alert/imk-alert.component";

@Component({
    moduleId: module.id,
    selector: 'imk-cart',
    templateUrl: 'cart.component.html',
    directives: [ROUTER_DIRECTIVES,ImkConfirmBtnComponent]
})

export class IMKCartComponent implements [OnInit, OnDestroy]{
    cartData: any = {_id:""};
    cartTotal: number = 0;
    loading: boolean = false;
    btn: string = 'Place Order';
    constructor(
        public loader: LoaderService,
        private _cart: IMKCartService,
        private _zone: NgZone,
        private _alert: ImkAlertService
    ){
        this._zone.run(()=>{
          this.cartData = this._cart.getData();
           for(var c in this.cartData){
               this.cartTotal += this.cartData[c].price * this.cartData[c].quantity;
           }
            //console.log(this.cartData);
        })
    }
    ngOnInit(){
        this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
    removeItem(item){
        this._cart.removeItem(item);
    }
    editItem(item){
        item.editItem = !item.editItem;
    }
    placeOrder(){
        this.loading = true;
        this.btn = 'Placing order please wait..';
        Meteor.call('placeOrderMediaKit',this.cartData,(err,result)=>{
            if(err){
                console.log(err.reason);
            } else {
                if(result.success === true){
                   this._zone.run(()=>{
                       this.cartData = {_id:""};
                       this._cart.resetCart();
                       this.loading = false;
                       this.btn = 'Place Order';
                   });
                    this._alert.success(result.message);
                } else {
                    this._alert.error(result.message);
                }

            }
        })
    }
}
