/**
 * Created by d3v on 6/2/16.
 */

import 'reflect-metadata';
import { Component, OnInit,OnDestroy } from '@angular/core';
import { LoaderService } from "../../../shared/loader/loader.service";
import { MeteorComponent } from "angular2-meteor/build/index";
import { MEDIA_KIT_PACKAGES } from "../../../../../collections/mediaKit/package";
import { ImkConfirmBtnComponent } from "../../../shared/imk-confirm-btn/imk-confirm-btn.component";
import {ImkAlertService} from "../../../shared/imk-alert/imk-alert.component";

@Component({
    moduleId: module.id,
    selector: 'media-kit-package-list',
    templateUrl: 'package-list.component.html',
    directives: [ImkConfirmBtnComponent]
})

export class MediaKitPackageListComponent extends MeteorComponent implements [OnInit,OnDestroy]{
    mkPackageList: Mongo.Cursor;
    constructor(
        public loader: LoaderService,
        private _alert: ImkAlertService
    ){
        super();
        this.autorun(()=>{
            this.subscribe('mediaKitPackages',()=>{
                this.mkPackageList = MEDIA_KIT_PACKAGES.find();
            },true)
        })
    }
    ngOnInit(){
        this.loader.stop()
    }
    ngOnDestroy(){
        this.loader.start();
    }
    delPackage(pl){
        Meteor.call('removeMediaKitPackage',pl._id,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success === true){
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message);
                }
            }
        })
    }
}