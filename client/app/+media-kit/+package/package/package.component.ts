/**
 * Created by d3v on 6/2/16.
 */

import 'reflect-metadata';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MeteorComponent } from "angular2-meteor/build/index";
import { FormBuilder, ControlGroup, Validators, Control } from '@angular/common';
import { MEDIA_KIT_CATEGORIES } from "../../../../../collections/mediaKit/categories";
import { LoaderService } from "../../../shared/loader/loader.service";
import { ImkConfirmBtnComponent } from "../../../shared/imk-confirm-btn/imk-confirm-btn.component";
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";

@Component({
    moduleId: module.id,
    selector: 'new-media-kit-package',
    templateUrl: 'package.component.html',
    directives:[ImkConfirmBtnComponent]
})

export class NewMediaKitPackageComponent extends MeteorComponent  implements [OnInit, OnDestroy]{
    newPackageForm : ControlGroup;
    package: any;
    packageDetail = [];
    mediaKitCategories: any;
    constructor(
        public loader: LoaderService,
        public _alert: ImkAlertService
    ){
        super();
        let fb = new FormBuilder();
        this.newPackageForm = fb.group({
            title:['',Validators.required],
            package:[this.packageDetail,Validators.required]
        });
        this.autorun(()=>{
            this.subscribe('MediaKitCategories',()=>{
                this.mediaKitCategories = MEDIA_KIT_CATEGORIES.find().fetch();
            },true)
        })
    }
    ngOnInit(){
        this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
    newPackage(mkPackage){
        console.log(mkPackage);
        Meteor.call('addMediaKitPackage',mkPackage,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success === true){
                    this._alert.success(result.message);
                }
                else {
                    this._alert.error(result.message);
                }
            }
        })
    }
    addToPackage(category,quantity){
        var _pd = this.mediaKitCategories;
        //console.log(_pd,category,quantity);
        //return;
        var _p = this.packageDetail;
        for(var p in _p){
            if(_p[p].categoryId === category){
                this._alert.error('Item already added to package');
                return;
            }
        }
        for(var c in _pd){
            if(_pd[c]._id === category){
                this.packageDetail.push({
                    categoryId:category,
                    categoryTitle:_pd[c].title,
                    quantity: quantity
                })
                return;
            }
        }
    }
    delPD(pd){
        this.packageDetail.splice(this.packageDetail.indexOf(pd),1);
    }
}