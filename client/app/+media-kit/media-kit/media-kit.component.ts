/**
 * Created by d3v on 6/1/16.
 */

import 'reflect-metadata';
import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { LoaderService } from "../../shared/loader/loader.service";
import { FormBuilder, ControlGroup, Validators, Control } from '@angular/common';
import { MediaKit } from '../mediaKit';
import { CropAndUploadImageComponent } from "../../+images/image/image.component";
import { UploadedImageListComponent } from "../../+images/image-list/image-list.component";
import { MeteorComponent } from 'angular2-meteor';
import { MEDIA_KIT_CATEGORIES } from '../../../../collections/mediaKit/categories';
import { GROUPS } from "../../../../collections/groups";
import { ImkAlertService } from "../../shared/imk-alert/imk-alert.component";

@Component({
    moduleId: module.id,
    selector: 'new-media-kit',
    templateUrl: 'media-kit.component.html',
    directives:[ CropAndUploadImageComponent, UploadedImageListComponent ],
    providers: [ImkAlertService]
})

export class NewMediaKitComponent extends MeteorComponent implements [OnInit, OnDestroy] {
    mediaKitForm: ControlGroup;
    mediaKit: any = MediaKit;
    mediaKitImage: String;
    mediaKitImageId: String;
    mediaKitCategories: Mongo.Cursor;
    groupList: any;
    selectedGroups = [];
    sizeAndPrice = [];
    constructor(
        public loader: LoaderService,
        private _zone: NgZone,
        private _alert : ImkAlertService
    ){
        super();
        let fb = new FormBuilder();
        this.mediaKitForm = fb.group({
            title: ['',Validators.required],
            description: ['',Validators.required],
            price:[''],
            categoryId: ['',Validators.required],
            imageUrl: [this.mediaKitImage],
            groups:[this.selectedGroups,Validators.required],
            size:[''],
            sizeAndPrice:[this.sizeAndPrice,Validators.required]
        });

        this.autorun(()=>{
            this.subscribe('MediaKitCategories',()=>{
                this.mediaKitCategories = MEDIA_KIT_CATEGORIES.find();
            },true)
            let _user = Meteor.user();
            if(_user){
                if(Roles.userIsInRole(_user,'super-admin',Roles.GLOBAL_GROUP)){
                    this.subscribe('groups', () => {
                        this.groupList =  GROUPS.find();
                    },true);
                }
                else{
                    Meteor.call('getGroupsForUser',(err,result)=>{
                        if(err){
                            console.log(err);
                        }
                        else{
                            this.groupList = result;
                        }
                    })
                }
            }
        })

    }
    ngOnInit(){
        this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
    toggle(item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) list.splice(idx, 1);
        else list.push(item);
    }
    exists(item, list) {
        return list.indexOf(item) > -1;
    }
    addMediaKit(media){
        media.mediaKitImgId = this.mediaKitImageId;
        Meteor.call('addMediaKit',media,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            } else {
                if(result.success === true){
                    this._alert.success(result.message);
                    (this.mediaKitForm.controls['title']).updateValue('');
                    (this.mediaKitForm.controls['description']).updateValue('');
                    (this.mediaKitForm.controls['price']).updateValue('');
                    (this.mediaKitForm.controls['size']).updateValue('');


                    this._zone.run(()=>{
                        this.sizeAndPrice = [];
                        this.mediaKitImage = '/img/imageph.png'
                        this.selectedGroups = [];
                    })


                }
                else{
                    this._alert.error(result.message);
                }
            }
        })
    }
    updateImages(event){
        this.mediaKitImage = event.url;
        this.mediaKitImageId = event._id;
    }
    addSP(size,price){
        this.sizeAndPrice.push({size:size,price:price});
        (this.mediaKitForm.controls['size']).updateValue('');
        (this.mediaKitForm.controls['price']).updateValue('');
    }
    removeSP(sp){
        this.sizeAndPrice.splice(this.sizeAndPrice.indexOf(sp),1);
    }
}