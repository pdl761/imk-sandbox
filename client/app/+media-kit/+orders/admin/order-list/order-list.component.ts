/**
 * Created by d3v on 6/6/16.
 */

import 'reflect-metadata';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoaderService } from "../../../../shared/loader/loader.service";
import { MeteorComponent } from "angular2-meteor/build/index";
import { USER_ORDERS } from "../../../../../../collections/userOrders";
import { Router, ROUTER_DIRECTIVES } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'media-kit-order-list',
    templateUrl :'order-list.component.html',
    directives: [ROUTER_DIRECTIVES]
})

export class AdminMediaKitOrderListComponent extends MeteorComponent implements [OnInit, OnDestroy]{
    userOrderList: Mongo.Cursor;
    constructor(
        public loader: LoaderService
    ){
        super();
        this.autorun(()=>{
            this.subscribe('getUserOrders',()=>{
                this.userOrderList = USER_ORDERS.find();
                console.log(this.userOrderList);
            },true);
        })
    }
    ngOnInit(){
        this.loader.stop()
    }
    ngOnDestroy(){
        this.loader.start();
    }
}