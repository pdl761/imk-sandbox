/**
 * Created by d3v on 6/7/16.
 */

import 'reflect-metadata';
import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { LoaderService } from "../../../../shared/loader/loader.service";
import { MeteorComponent } from "angular2-meteor/build/index";
import { RouteSegment } from '@angular/router';
import { ImkConfirmBtnComponent } from "../../../../shared/imk-confirm-btn/imk-confirm-btn.component";
import { ImkAlertService } from "../../../../shared/imk-alert/imk-alert.component";
import { USER_ORDERS } from "../../../../../../collections/userOrders";
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'media-kit-order-details',
    templateUrl: 'order-detail.component.html',
    directives: [ImkConfirmBtnComponent,ROUTER_DIRECTIVES]
})

export class AdminUserMediaKitOrderDetailComponent extends MeteorComponent implements [OnInit, OnDestroy]{
    orderId: string;
    orderDetail : any;
    loggedInUserId:string;
    constructor(
        public loader: LoaderService,
        private _param : RouteSegment,
        private _zone: NgZone,
        private _alert: ImkAlertService
    ){
        super();
        this.orderId = this._param.getParam('id');
        this.loggedInUserId = Meteor.userId();
        //this.autorun(()=>{
        //    this.subscribe('getOrderDetailById',this.orderId,()=>{
        //        this.orderDetail = USER_ORDERS.find();
        //        this.orderDetail = this.orderDetail;
        //    },true);
        //})

        Meteor.call('getOrderDetailById',this.orderId,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            } else {
                if(result.success === true){
                    this._zone.run(()=>{
                        this.orderDetail = result.data[0];
                    })
                } else {
                    this._alert.error(result.message);
                }
                //console.log(result);
            }
        })
    }
    ngOnInit(){
        this.loader.stop()
    }
    ngOnDestroy(){
        this.loader.start();
    }
    approveOrderItem(orderId,itemId){
        //console.log(orderId,itemId);
        Meteor.call('approveOrderItem',orderId,itemId,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            } else {
                if(result.success === true){
                    this._alert.success(result.message);
                    let order = this.orderDetail.orders;
                    for(var o in order){
                        if(order[o].mediaKitId === itemId){
                            this._zone.run(()=>{
                                order[o].status = 1;
                            });
                            console.log(this.orderDetail);
                            return;
                        }
                    }
                } else {
                    this._alert.error(result.message);
                }
            }

        })
    }
    rejectOrderItem(orderId,itemId){
        //console.log(orderId,itemId);
        Meteor.call('rejectOrderItem',orderId,itemId,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            } else {
                if(result.success === true){
                    this._alert.success(result.message);
                    let order = this.orderDetail.orders;
                    for(var o in order){
                        if(order[o].mediaKitId === itemId){
                            this._zone.run(()=>{
                                order[o].status = 2;
                            });
                            console.log(this.orderDetail);
                            return;
                        }
                    }
                } else {
                    this._alert.error(result.message);
                }
            }

        })
    }

}