/**
 * Created by d3v on 6/6/16.
 */

import 'reflect-metadata';
import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { LoaderService } from "../../../../shared/loader/loader.service";
import { MeteorComponent } from "angular2-meteor/build/index";
import { USER_ORDERS } from "../../../../../../collections/userOrders";
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'user-media-kit-order-list',
    templateUrl: 'order-list-user.component.html',
    directives:[ROUTER_DIRECTIVES]
})

export class UserMediaKitOrderListComponent extends MeteorComponent implements [OnInit, OnDestroy]{
    myOrders: Mongo.Cursor;
    constructor(
        private loader: LoaderService,
        private _zone: NgZone
    ){
        super();
        this.autorun(()=>{
            this.subscribe('getMyOrders',()=>{
                this._zone.run(()=>{
                    this.myOrders = USER_ORDERS.find();
                });
                console.log(this.myOrders);
            },true);
        });
    }
    ngOnInit(){
        this.loader.stop()
    }
    ngOnDestroy(){
        this.loader.start();
    }


}
