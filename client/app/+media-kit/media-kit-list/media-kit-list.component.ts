/**
 * Created by d3v on 6/1/16.
 */

import 'reflect-metadata';
import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { LoaderService } from "../../shared/loader/loader.service";
import { MeteorComponent } from 'angular2-meteor';
import { MEDIA_KIT } from '../../../../collections/mediaKit/mediakit';
import { ImkAlertService } from "../../shared/imk-alert/imk-alert.component";
import {MEDIA_KIT_CATEGORIES} from "../../../../collections/mediaKit/categories";
import {Thumbs} from "../../../../collections/+api/image/collection";

@Component({
    moduleId: module.id,
    selector: 'media-kit-list',
    templateUrl: 'media-kit-list.component.html',
    directives: [ROUTER_DIRECTIVES],
    providers:[ImkAlertService]
})

export class MediaKitListComponent extends MeteorComponent implements [OnInit, OnDestroy]{
    mediaKit: any;
    mediaKitList: any;
    constructor(
        public loader: LoaderService,
        private _alert: ImkAlertService,
        private _zone: NgZone
    ){
        super();
        this.autorun(()=>{
            this.subscribe('mediaKits',()=>{
                this.mediaKitList = MEDIA_KIT.find();
               // console.log(this.mediaKitList);
            },true)
        })
    }
    ngOnInit(){
        this.loader.stop()
    }
    ngOnDestroy(){
        this.loader.start();
    }
    //removeMedia(id){
    //    Meteor.call('removeMediaKitById',id,(err,result)=>{
    //        if(err){
    //            this._alert.error(err.reason);
    //        }
    //        else{
    //            if(result.success === true){
    //                this._alert.success(result.message);
    //            }
    //            else{
    //                this._alert.error(result.message);
    //            }
    //        }
    //    })
    //}
}