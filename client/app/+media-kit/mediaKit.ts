/**
 * Created by d3v on 6/1/16.
 */

export let MediaKit = {
    _id :"",
    title: "",
    description: "",
    categoryId:"",
    categoryTitle:"",
    thumbnailUrl:"",
    imageUrl:"",
    isDeleted:false
}