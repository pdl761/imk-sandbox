/**
 * Created by d3v on 6/1/16.
 */

import 'reflect-metadata';
import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { ROUTER_DIRECTIVES,RouteSegment, Router } from '@angular/router';
import { LoaderService } from "../../shared/loader/loader.service";
import { MeteorComponent } from 'angular2-meteor';
import { ImkAlertService } from "../../shared/imk-alert/imk-alert.component";
import { IMKCartService } from "../../shared/cart/cart.service";
import { FormBuilder, ControlGroup, Validators, Control } from '@angular/common';
import { RadioControlValueAccessor } from "../../shared/radio-value-accessor/index";

@Component({
    moduleId: module.id,
    selector: 'media-kit-detail',
    templateUrl: 'media-kit-detail.component.html',
    directives: [ROUTER_DIRECTIVES,RadioControlValueAccessor],
    providers:[ImkAlertService]
})

export class MediaKitDetailComponent extends MeteorComponent implements [OnInit, OnDestroy]{
    mediaKitId : string;
    mediaKit: any;

    branches:any;
    vendors: any;
    user:any;
    orderedMedia :any ;
    addMediaKitForm: ControlGroup;
    btn: string = 'Add to cart';
    orderTitle: string = 'Place Order';
    editMode: boolean = false;

    userProfile: any = {userRoles:'',isSU:''};

    constructor(
        public loader: LoaderService,
        private _param:RouteSegment,
        private _zone: NgZone,
        private _alert: ImkAlertService,
        private _router: Router,
        private _cart: IMKCartService


    ){
        super();

        Meteor.call('getUserRoles',(err,result)=>{
            if(err){}
            else {
                this.userProfile.userRoles = result;
                this.userProfile.isSU = Roles.userIsInRole(Meteor.userId(),'super-admin',Roles.GLOBAL_GROUP);
                console.log(this.userProfile);
            }
        })

        this.orderedMedia = {
            requestedChanges:  "",
            size:  "",
            quantity: "",
            branch: "download",
            vendor: "any"
        };


        let fb = new FormBuilder();
        this.addMediaKitForm = fb.group({
            requestedChanges: ['',Validators.required],
            size: ['',Validators.required],
            quantity:['',Validators.required],
            branch:[this.orderedMedia.branch,Validators.required],
            vendor:[this.orderedMedia.vendor,Validators.required]
        });

            this.mediaKitId = this._param.getParam('id');
            this.editOrderDetails();

            Meteor.call('getMediaKitWithVendorsAndBranches',this.mediaKitId,(err,result)=>{
                if(err){ console.log(err.reason);}
                else{
                    console.log(result);
                    if(result){
                        this._zone.run(()=>{
                            this.mediaKit = result;
                            this.branches = result.branches;
                            this.vendors = result.vendors;
                        })
                    }

                }
            });


    }
    ngOnInit(){
        this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
    editOrderDetails(){
        var selectedItem = this._cart.getData();
        if(selectedItem.length >0){
            for(var order in selectedItem){
                if(selectedItem[order].itemId === this.mediaKitId){
                    this.orderedMedia.requestedChanges = selectedItem[order].requestedChanges;
                    this.orderedMedia.size = selectedItem[order].size;
                    this.orderedMedia.quantity = selectedItem[order].quantity;
                    this.orderedMedia.branch = selectedItem[order].branch;
                    this.orderedMedia.vendor = selectedItem[order].vendor;
                    this.editMode = true;
                    this.orderTitle = 'Edit Order';
                    this.btn = 'Update';
                    return;
                }
            }
        }
    }
    removeMedia(id){
        Meteor.call('removeMediaKitById',id,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success === true){
                    this._alert.success(result.message);
                    this._router.navigate(['/dashboard/media-kit/list']);
                }
                else{
                    this._alert.error(result.message);
                }
            }
        })
    }
    attachBranchDetails(media){
        var branches = this.branches;
        for (var b in branches){
            if(branches[b]._id === media.branch){
                media.branchName = branches[b].name;
                if(branches[b].address){
                    media.branchAddress = branches[b].address;
                }

                return media;
            }
        }
    }
    attachVendorDetails(media){
        var vendors = this.vendors;
        for (var v in vendors){
            if(vendors[v]._id === media.vendor){
                media.vendorName = vendors[v].profile.firstName + " " + vendors[v].profile.lastName;
                if(vendors[v].profile.address){
                    media.vendorAddress = vendors[v].profile.address.office;
                }
                return media;
            }
        }
    }
    getPrice(media){
        var item = this.mediaKit.sizeAndPrice;
        for(var i in item){
            if(item[i].size === media.size){
                media.price = item[i].price;
                return media;
            }
        }
    }
    addToCart(media){
        console.log(media);
        if(media.branch !== 'download'){
            this.attachBranchDetails(media);
        }
        if(media.vendor !=='vendor'){
            this.attachVendorDetails(media);
        }
        this.getPrice(media);
        console.log(media);
        media.itemId = this.mediaKitId;
        media.name = this.mediaKit.title;
        media.image = this.mediaKit.image.url;
        media.category = this.mediaKit.category.title;
        var result = this._cart.setData(media);
        if(result.success === true){
            //this.changeRequest = "";
            //this.changeRequestSize = "";
            this._alert.success('Item added to cart successfully');
            return;
        }
        if(result.success === false && result.reason === 'exists'){
            this._alert.error('Item already added to cart. If you want to edit your order please check your cart');
            return;
        }
        else{
            this._alert.error('Error while adding item to cart');
            return;
        }
    }
}