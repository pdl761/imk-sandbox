/**
 * Created by gurpinder on 5/14/16.
 */

import 'reflect-metadata';
import { Component,OnInit,OnDestroy } from '@angular/core';
import { LoaderService } from "../shared/loader/loader.service";

@Component({
    moduleId: module.id,
    selector: 'dashboard-home',
    templateUrl: 'dashboard-index.component.html'
})

export class DashboardHome implements [OnInit,OnDestroy]{
    user: any;
    constructor(public loader: LoaderService
    ){
        this.user = Meteor.user();
        console.log(this.user);
    }
    ngOnInit(){
        this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
}
