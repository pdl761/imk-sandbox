/**
 * Created by gurpinder on 5/13/16.
 */

import 'reflect-metadata';
import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES, Routes, Router } from '@angular/router';
import { UserComponent } from "../+users/user/user.component";
import { NavbarTopComponent } from "../shared/navbar/navbar.component";
import { DashboardHome } from "./dashboard-index.component";
import { RolesComponent } from "../+roles/roles.component";
import { GroupsComponent } from "../+groups/groups.component";
import { UserListComponent } from "../+users/user-list/user-list.component";
import { CategoriesComponent } from "../+blog/+categories/categories.component";
import { PostComponent } from "../+blog/+posts/post/post.component";
import { PostListComponent } from "../+blog/+posts/post-list/post-list.component";
import { CommentsComponent } from "../+blog/+comments/comments.component";
import { UserDetailComponent } from "../+users/user-detail/user-detail.component";
import { EditUserComponent } from "../+users/+edit-user/edit-user.component";
import { ContactListComponent } from "../+leads/+contacts/contact-list/contact-list.component";
import { ContactDetailComponent } from "../+leads/+contacts/contact-detail/contact-detail.component";
import { NewMediaKitComponent } from "../+media-kit/media-kit/media-kit.component";
import { MediaKitListComponent } from "../+media-kit/media-kit-list/media-kit-list.component";
import { MediaKitDetailComponent } from "../+media-kit/media-kit-detail/media-kit-detail.component";
import { IMKCartComponent } from "../+cart/cart/cart.component";
import { NewMediaKitPackageComponent } from "../+media-kit/+package/package/package.component";
import { MediaKitPackageListComponent } from "../+media-kit/+package/package-list/package-list.component";
import { AdminMediaKitOrderListComponent } from "../+media-kit/+orders/admin/order-list/order-list.component";
import { UserMediaKitOrderListComponent } from "../+media-kit/+orders/user/order-list/order-list-user.component";
import {AdminUserMediaKitOrderDetailComponent} from "../+media-kit/+orders/admin/order-detail/order-detail.component";

@Component({
    moduleId: module.id,
    selector: 'dashboard',
    templateUrl: 'dashboard.component.html',
    directives: [ROUTER_DIRECTIVES, NavbarTopComponent, PostListComponent]
})
@Routes([
    { path: '/', component: DashboardHome },
    { path: '/roles', component: RolesComponent },
    { path: '/groups', component: GroupsComponent },
    { path: '/add-new-user', component: UserComponent },
    { path: '/user-list', component: UserListComponent },
    { path: '/categories', component: CategoriesComponent },
    { path: '/create-new-post', component: PostComponent },
    { path: '/post-list', component: PostListComponent },
    { path: '/edit-post/:postId', component: PostComponent },
    { path: '/comments/:postId', component: CommentsComponent },
    { path: '/user/:userId', component: UserDetailComponent },
    { path: '/edit-user/:userId',component: EditUserComponent },
    { path: '/contact-message-list', component: ContactListComponent },
    { path: '/contact-message/:contactId', component: ContactDetailComponent },
    { path: '/media-kit/add-new', component: NewMediaKitComponent },
    { path: '/media-kit/list', component: MediaKitListComponent },
    { path: '/media-kit/detail/:id', component: MediaKitDetailComponent },
    { path: '/cart', component: IMKCartComponent },
    { path: '/media-kit/new-package', component: NewMediaKitPackageComponent},
    { path: '/media-kit/package-list', component: MediaKitPackageListComponent },
    { path: '/media-kit/order-list', component: AdminMediaKitOrderListComponent },
    { path: '/media-kit/order-detail/:id', component: AdminUserMediaKitOrderDetailComponent },
    { path: '/media-kit/user/order-list', component: UserMediaKitOrderListComponent },

])
export class DashboardComponent {}