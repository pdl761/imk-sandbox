/**
 * Created by gurpinder on 5/14/16.
 */

import 'reflect-metadata';
import { Component, NgZone } from '@angular/core';
import { MeteorComponent } from 'angular2-meteor';
import { ImkAlertService } from "../../shared/imk-alert/imk-alert.component";
import { ImkConfirmBtnComponent } from "../../shared/imk-confirm-btn/imk-confirm-btn.component";

@Component({
    moduleId: module.id,
    selector: 'role-list',
    templateUrl: 'role-list.component.html',
    directives: [ImkConfirmBtnComponent],
    providers: [ImkAlertService]
})

export class RoleListComponent extends MeteorComponent{
    roles: Mongo.Cursor;
    constructor(
        private _ngZone: NgZone,
        private _alert: ImkAlertService
    ){
        super();
        this.autorun(()=>{
            this.subscribe('roles', () => {
                this.roles = Meteor.roles.find();
            },true);
        });

    }
    delRole(name){
        Meteor.call('deleteRole',name,(err,result)=>{
            if(err){
                //console.log(err);
                this._alert.error(err.reason);
            }
            else{
                //console.log(result);
                if(result.success == true){
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message)
                }
            }
        });
    }
}