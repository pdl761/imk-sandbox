/**
 * Created by gurpinder on 5/14/16.
 */

import 'reflect-metadata';
import { Component, NgZone } from '@angular/core';
import { MeteorComponent } from 'angular2-meteor';
import { FormBuilder, ControlGroup, Validators, Control } from '@angular/common';
import { ImkAlertService } from '../../shared/imk-alert/imk-alert.component';

@Component({
    moduleId: module.id,
    selector: 'create-role',
    templateUrl: 'role.component.html',
    providers: [ImkAlertService]
})

export class RoleComponent extends MeteorComponent{
    roleForm: ControlGroup;
    constructor(
        private _zone : NgZone,
        private _alert: ImkAlertService
    ){
        super();
        let fb = new FormBuilder();
        this.roleForm = fb.group({
            roleName: ['',Validators.required]
        });
    }
    createRole(role): void{
        Meteor.call('createRole',role.roleName,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                //console.log(result);
                if(result.success == true){
                    (this.roleForm.controls['roleName']).updateValue('');
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message);
                }

            }
        });
    }
}