/**
 * Created by gurpinder on 5/14/16.
 */

import 'reflect-metadata';
import { Component } from '@angular/core';
import { RoleComponent } from "./role/role.component";
import { RoleListComponent } from "./role-list/role-list.component";

@Component({
    moduleId: module.id,
    selector: 'roles',
    templateUrl: 'roles.component.html',
    directives: [RoleComponent, RoleListComponent]
})

export class RolesComponent {}