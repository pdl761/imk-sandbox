/**
 * Created by d3v on 5/23/16.
 */

import 'reflect-metadata';
import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'contact-form',
    templateUrl: 'contact.component.html'
})

export class ContactFormComponent {}