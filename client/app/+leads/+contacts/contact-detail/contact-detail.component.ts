/**
 * Created by d3v on 5/23/16.
 */

import 'reflect-metadata';
import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { ROUTER_DIRECTIVES, RouteSegment, Router } from  '@angular/router';
import { MeteorComponent } from 'angular2-meteor';
import { LEADS } from "../../../../../collections/leads";
import { ImkConfirmBtnComponent } from "../../../shared/imk-confirm-btn/imk-confirm-btn.component";
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";
import { LoaderService } from "../../../shared/loader/loader.service";

@Component({
    moduleId: module.id,
    selector: 'contact-detail',
    templateUrl: 'contact-detail.component.html',
    directives: [ROUTER_DIRECTIVES,ImkConfirmBtnComponent],
    providers: [ImkAlertService]
})

export class ContactDetailComponent extends MeteorComponent implements [OnInit,OnDestroy]{
    contactId: string;
    leadContact : any;
    constructor(
        private zone: NgZone,
        private router : Router,
        private routerParam : RouteSegment,
        private _alert: ImkAlertService,
        public loader: LoaderService
    ){
        super();
        this.contactId = this.routerParam.getParam('contactId');
        this.autorun(()=>{
            this.subscribe('getLeadContact',()=>{
                this.leadContact = LEADS.find({_id:this.contactId});
            },true);
        });
    }
    ngOnInit(){
        this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
    delMessage(id){
        Meteor.call('deleteLeadContact',id,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    this._alert.success(result.message);
                    this.zone.run(()=>{
                        this.router.navigate(['/dashboard/contact-message-list']);
                    })
                }
                else{
                    this._alert.error(result.message)
                }
            }
        });
    }
}