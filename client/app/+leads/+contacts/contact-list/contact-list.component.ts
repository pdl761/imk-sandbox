/**
 * Created by d3v on 5/23/16.
 */

import 'reflect-metadata';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MeteorComponent } from 'angular2-meteor';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { LEADS } from '../../../../../collections/leads';
import { ImkConfirmBtnComponent } from "../../../shared/imk-confirm-btn/imk-confirm-btn.component";
import { ImkAlertService } from "../../../shared/imk-alert/imk-alert.component";
import { LoaderService } from "../../../shared/loader/loader.service";

@Component({
    moduleId: module.id,
    selector: 'contact-list',
    templateUrl: 'contact-list.component.html',
    directives: [ROUTER_DIRECTIVES, ImkConfirmBtnComponent],
    providers: [ImkAlertService]
})

export class ContactListComponent extends MeteorComponent implements [OnInit,OnDestroy]{
    contactList : Mongo.Cursor;
    constructor(
        private _alert: ImkAlertService,
        public loader: LoaderService
    ){
        super();
        this.autorun(()=>{
            this.subscribe('leadsContact',()=>{
               this.contactList = LEADS.find();
            },true);
        })
    }
    ngOnInit(){
        this.loader.stop()
    }

    ngOnDestroy(){
        this.loader.start();
    }
    delMessage(id){
        Meteor.call('deleteLeadContact',id,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
                if(result.success == true){
                    this._alert.success(result.message);
                }
                else{
                    this._alert.error(result.message)
                }
            }
        });
    }
}