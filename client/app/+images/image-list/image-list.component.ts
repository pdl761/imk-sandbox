/**
 * Created by d3v on 5/19/16.
 */

import 'reflect-metadata';
import { Component,Output, EventEmitter } from '@angular/core';
import { MeteorComponent } from 'angular2-meteor';
import { ImageCropperComponent, CropperSettings } from '../../libs/ng2-img-cropper/src/imageCropper';
import { Images } from '../../../../collections/+api/image/collection';
import { ImkAlertService } from "../../shared/imk-alert/imk-alert.component";
import { ImkConfirmBtnComponent } from "../../shared/imk-confirm-btn/imk-confirm-btn.component";
@Component({
    moduleId: module.id,
    selector: 'uploaded-image-list',
    templateUrl: 'image-list.component.html',
    directives: [ImageCropperComponent,ImkConfirmBtnComponent],
    providers: [ImkAlertService]
})

export class UploadedImageListComponent extends MeteorComponent{

    imageList:  Mongo.Cursor;

    @Output() selectedImageUrl = new EventEmitter<Object>();

    constructor(private _alert: ImkAlertService){
        super();
        this.autorun(()=>{
            this.subscribe('uploadedImageList',()=>{
                this.imageList = Images.find();

            },true);
        });
    }
    delImage(id){
        Meteor.call('delUploadedImages',id,(err,result)=>{
            if(err){
                this._alert.error(err.reason);
            }
            else{
               if(result.success == true){
                   this._alert.success(result.message);
               }
                else{
                   this._alert.error(result.message);
               }
            }
        })
    }
    selectImage(url){
        $('.imageUploadModal').modal('hide');
        this.selectedImageUrl.emit(url);
    }
}