/**
 * Created by d3v on 5/18/16.
 */

import 'reflect-metadata';
import { Component,Input, Output ,EventEmitter, OnInit, NgZone } from '@angular/core';
import { ImageCropperComponent, CropperSettings } from '../../libs/ng2-img-cropper/src/imageCropper';
import { upload } from '../../../../collections/+api/image/methods';
import { ImkAlertService } from "../../shared/imk-alert/imk-alert.component";

@Component({
    moduleId: module.id,
    selector: 'upload-image',
    templateUrl: 'image.component.html',
    directives: [ImageCropperComponent],
    providers: [ImkAlertService]
})

export class CropAndUploadImageComponent implements OnInit{
    status: String = "Upload";
    isUploading: boolean = false;

    @Input() cwidth  :any;
    @Input() cheight :any ;
    @Input() croppedWidth;
    @Input() croppedHeight :any;
    @Input() canvasWidth :any;
    @Input() canvasHeight :any;

    @Output() croppedImage = new EventEmitter<string>();
    @Output() uploadedImageUrl = new EventEmitter<Object>();

    onChange(value: string){
        //console.log(this.canvasWidth);
        //this.croppedImage.emit(value);
    }
    imageData: any;
    cropperSettings: CropperSettings;
    constructor(
        private _alert: ImkAlertService,
        private _zone: NgZone
    ){
    }
    ngOnInit(){
        this.cropperSettings = new CropperSettings();
        this.cropperSettings.width = this.cwidth;
        this.cropperSettings.height = this.cheight;
        this.cropperSettings.croppedWidth = this.croppedWidth;
        this.cropperSettings.croppedHeight = this.croppedHeight;
        this.cropperSettings.canvasWidth = this.canvasWidth;
        this.cropperSettings.canvasHeight = this.canvasHeight;
        this.imageData = {};
        this.imageData.image = 'http://ingridwu.dmmdmcfatter.com/wp-content/uploads/2015/01/placeholder.png';
    }
    save(){
        this.isUploading = true;
        this.status = "Uploading image..";
        upload(this.imageData.image, this.imageData.name, ((file) => {
            if(file){
                this._zone.run(()=>{
                    this.uploadedImageUrl.emit(file);
                    $('.imageUploadModal').modal('hide');
                    //console.log(file);
                    this._alert.success('File Uploaded Successfully');
                    this.isUploading = false;
                    this.status = "Upload";
                })
            }

        }), (e) => {
            this._alert.error('Oops, something went wrong: '+ e);
        });
    }

    reset() {

    }
}