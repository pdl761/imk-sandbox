/**
 * Created by d3v on 5/23/16.
 */

import {Schemas} from './schemas';

export let LEADS = new Mongo.Collection('leads');

Schemas.Lead = new SimpleSchema({
    userName: {type: String,optional:true},
    userEmail: {type:String},
    userPhone: {type:String,optional:true},
    userMessage: {type:String,optional:true},
    userId: {type:String},
    type: {type:String,optional:true},
    meta: {type: Object,optional:true},
    createdAt: {type:Date,optional:true},
    isDeleted: {type:Boolean,optional:true}
})

LEADS.attachSchema(Schemas.Lead);