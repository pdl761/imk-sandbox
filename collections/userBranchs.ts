/**
 * Created by d3v on 6/6/16.
 */

import { Schemas } from './schemas';

export let USER_BRANCHES = new Mongo.Collection('userBranches');

Schemas.UserBranches  = new SimpleSchema({
    userId: { type:String,optional:true},
    name: { type:String },
    address:{type:Object},
    group:{type:String,optional:true},
    'address.street':{type:String},
    'address.city':{type:String},
    'address.state':{type:String},
    'address.zipCode':{type:Number},
    contact:{type:Object},
    'contact.phone':{type:Number},
    'contact.fax':{type:Number},
    'contact.email':{type:String,optional:true},
    createdAt: { type:String, optional:true }
})

USER_BRANCHES.attachSchema(Schemas.UserBranches);