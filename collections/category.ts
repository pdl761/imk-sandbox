/**
 * Created by d3v on 4/27/16.
 */

import {Schemas} from './schemas';

export let CATEGORIES = new Mongo.Collection('categories');

Schemas.Category = new SimpleSchema({
    title: {type: String},
    userId: {type:String},
    createdAt: {type:Date,optional:true},
    isDeleted: {type:Boolean,optional:true}
})

CATEGORIES.attachSchema(Schemas.Category);