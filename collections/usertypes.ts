/**
 * Created by d3v on 6/1/16.
 */

import { Schemas } from './schemas';

export let USER_TYPES = new Mongo.Collection('usertypes');

Schemas.usertypes = new SimpleSchema({
    title: { type:String },
    name: { type:String },
    isDeleted: {type:Boolean,optional:true}
})

USER_TYPES.attachSchema(Schemas.usertypes);