/**
 * Created by d3v on 5/24/16.
 */

import { Schemas } from './schemas';

export let PRODUCTS = new Mongo.Collection('products');

Schemas.Products  = new SimpleSchema({
    title: { type:String },
    name: { type:String },
    description: { type:String, optional:true },
    createdAt: { type:String, optional:true }
})

PRODUCTS.attachSchema(Schemas.Products);