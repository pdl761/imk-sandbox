/**
 * Created by gurpinder on 4/10/16.
 */

import {Schemas} from './schemas';

export let GROUPS = new Mongo.Collection('groups');

Schemas.Group = new SimpleSchema({
    title: {type: String},
    userId: {type:String},
    createdAt: {type:Date,optional:true},
    isDeleted: {type:Boolean,optional:true}
})

GROUPS.attachSchema(Schemas.Group);

