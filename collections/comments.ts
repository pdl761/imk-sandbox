/**
 * Created by d3v on 4/27/16.
 */


import {Schemas} from './schemas';

export let COMMENTS = new Mongo.Collection('comments');

Schemas.Comment = new SimpleSchema({
     postId: {type:String},
     userId: {type:String,optional: true},
     userName: {type:String},
     userEmail: {type:String},
     comment: {type:String},
     createdAt:{type:Date,optional: true},
     isApproved: {type:Boolean,optional: true},
     isDeleted: {type:Boolean,optional: true}
})

COMMENTS.attachSchema(Schemas.Comment);