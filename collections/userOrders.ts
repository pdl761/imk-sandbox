/**
 * Created by d3v on 6/7/16.
 */

import { Schemas } from './schemas';

export let USER_ORDERS = new Mongo.Collection('userOrders');

Schemas.UserOrders = new SimpleSchema({
    userId:{type:String,optional:true},
    orders:{type:[Object]},
    'orders.$.mediaKitId':{type:String,optional:true},
    'orders.$.requestedChanges':{type:String},
    'orders.$.size':{type:String},
    'orders.$.quantity':{type:Number},
    'orders.$.branchId':{type:String},
    'orders.$.vendorId':{type:String},
    'orders.$.status':{type:Number,optional:true},
    'orders.$.message':{type:String,optional:true},
    'orders.$.group':{type:String,optional:true},
    orderedAt:{type:Date,optional:true},
    orderedItemsCount:{type:Number,optional:true},
    isDeleted: {type:Boolean,optional:true},
    group:{type:String}
})

USER_ORDERS.attachSchema(Schemas.UserOrders);