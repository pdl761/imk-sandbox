/**
 * Created by d3v on 4/27/16.
 */

import {Schemas} from './schemas';

export let POSTS = new Mongo.Collection('posts');

Schemas.Post = new SimpleSchema({
    title: {type:String},
    featuredImageId: {type:String,optional:true},
    featuredImage: {type:String,optional:true},
    featuredImageThumb: {type:String,optional:true},
    body: {type:String},
    createdAt: {type:Date,optional:true},
    updatedAt:{type:Date,optional:true},
    tags:{type:[String]},
    categoryId:{type:String},
    userId:{type:String,optional:true},
    isPublished: {type:Boolean},
    isDeleted: {type:Boolean,optional:true},
    allowComments: {type:Boolean},
    isFeatured: {type:Boolean},
    groups: {type:[String]}
})

POSTS.attachSchema(Schemas.Post);

