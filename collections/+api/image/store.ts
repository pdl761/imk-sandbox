/**
 * Created by d3v on 5/18/16.
 */

import { Images, Thumbs } from './collection';

export const ThumbsStore = new UploadFS.store.GridFS({
    collection: Thumbs,
    name: 'thumbs',
    transformWrite(from, to, fileId, file) {
        // Resize to 200x200
        const gm = require('gm');

        gm(from, file.name)
            .resize(150, 150)
            //.gravity('Center')
            //.extent(200, 200)
            .quality(75)
            .stream()
            .pipe(to);
    }
});

export const ImagesStore = new UploadFS.store.GridFS({
    collection: Images,
    name: 'images',
    filter: new UploadFS.Filter({
        contentTypes: ['image/*']
    }),
    copyTo: [
        ThumbsStore
    ]
});