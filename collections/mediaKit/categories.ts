/**
 * Created by d3v on 6/1/16.
 */

import { Schemas } from './../schemas';

export let MEDIA_KIT_CATEGORIES = new Mongo.Collection('mediaKitCategories');

Schemas.MediaKitCategories = new SimpleSchema({
    title:{type:String},
    name: {type:String},
    isDeleted: {type:Boolean}
});

MEDIA_KIT_CATEGORIES.attachSchema(Schemas.MediaKitCategories);
