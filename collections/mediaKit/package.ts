/**
 * Created by d3v on 6/2/16.
 */

import { Schemas } from '../schemas';

export let MEDIA_KIT_PACKAGES = new Mongo.Collection('mediaKitPackages');

Schemas.MediaKitPackages = new SimpleSchema({
    title: {type:String},
    package:{type:[Object]},
    "package.$.categoryId":{type:String},
    "package.$.categoryTitle":{type:String},
    "package.$.quantity":{type:Number},
    createdAt: {type:Date,optional:true},
    userId: {type:String,optional:true},
    isDeleted:{type:String,optional:true}
})

MEDIA_KIT_PACKAGES.attachSchema(Schemas.MediaKitPackages);