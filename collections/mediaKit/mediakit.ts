/**
 * Created by d3v on 6/1/16.
 */

import { Schemas } from './../schemas';

export let MEDIA_KIT = new Mongo.Collection('mediaKit');

Schemas.MediaKit = new SimpleSchema({
    title: {type:String},
    description: {type:String},
    categoryId:{type:String},
    imageId:{type:String,optional:true},
    isDeleted:{type:Boolean,optional:true},
    userId:{type:String,optional:true},
    userType:{type:String,optional:true},
    groups: {type:[String]},
    addedAt:{type:Date,optional:true},
    updatedAt:{type:Date,optional:true},
    sizeAndPrice:{type:[Object],optional:true},
    "sizeAndPrice.$.size": {
        type: String
    },
    "sizeAndPrice.$.price": {
        type: Number
    }
})

MEDIA_KIT.attachSchema(Schemas.MediaKit);
